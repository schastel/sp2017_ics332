---
morea_id: outcomevirtualmemory
morea_type: outcome
title: "Virtual Memory - Paging"
published: True
morea_sort_order: 70
---

- Understand the need for the Virtual Memory abstraction
- Understand how Operating Systems implement Virtual Memory (paging, page tables)


