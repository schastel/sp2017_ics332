---
morea_id: reading-java
morea_type: reading
title: "Java and the command line"
published: True
morea_summary: "No need of an IDE to compile and execute Java"
morea_sort_order: 8
morea_labels: 
  - "Lecture notes"
  - "Java"
---
Since you may not be familiar with the use of java/javac on the cli.

 * Assume your sources are in the src directory.

 * You created HelloWorld.java in the package edu.hawaii.ics332.apps
   (i.e. there is a file src/edu/hawaii/ics332/apps/HelloWorld.java)

 * We assume that the file contents of HelloWorld.java are the following:
<pre>
package edu.hawaii.ics332.apps;

public class HelloWorld {
	public static void main(String[] args) {
		System.out.println("Hello, World!");
	}
}
</pre>

### Dirty way

It is not advised to use this

* You want to compile the file src/edu/hawaii/ics332/apps/HelloWorld.java
 <pre>
 javac -sourcepath src src/edu/hawaii/ics332/apps/HelloWorld.java
 </pre>
   The class file is generated in src/edu/hawaii/ics332/apps (in the sources directory)

 * You want to run the HelloWorld program:

<pre>
java -cp src edu.hawaii.ics332.apps.HelloWorld 
</pre>

### Clean way

* You want to compile the file
   src/edu/hawaii/ics332/apps/HelloWorld.java and place the class file
   in a directory (e.g. build) different from the sources directory
   (you don't want to delete the source file when cleaning up)
   
 <pre>
 <font color="red">mkdir build</font>
 javac -sourcepath src <font color="red">-d build</font> src/edu/hawaii/ics332/apps/HelloWorld.java
 </pre>

 Note the existence of build/edu/hawaii/ics332/apps/HelloWorld.class

 * You want to run the HelloWorld program:
<pre>
java -cp build edu.hawaii.ics332.apps.HelloWorld 
</pre>

### Clean way + dependencies on jar

 * Your HelloWorld program now depends on a library whose jar is named lib/tools.jar. Its source code is:

package edu.hawaii.ics332.apps;

import edu.hawaii.ics332.common.Tools;

public class HelloWorld {
	public static void main(String[] args) {
		System.out.println(Tools.cleanUp("Hello, World!"));
	}
}

 * To compile and generate the class file in the build directory:
<pre>
javac <font color="red">-classpath lib/tools.jar</font> -sourcepath src -d build src/edu/hawaii/ics332/apps/HelloWorld.java
</pre>

 * To execute:
<pre>
java -classpath lib/tools.jar:build edu.hawaii.ics332.apps.HelloWorld
</pre>

