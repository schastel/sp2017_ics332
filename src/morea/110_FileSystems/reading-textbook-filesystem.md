---
morea_id: textbook_filesystem
morea_type: reading
title: "Textbook Reading"
published: True
morea_summary: ""
morea_labels: 
  - "Reading"
---

* <a href="http://pages.cs.wisc.edu/~remzi/OSTEP/file-intro.pdf">39 - Files and Directories</a>

* <a href="http://pages.cs.wisc.edu/~remzi/OSTEP/file-implementation.pdf">40 - FS Implementation</a>

* <a href="http://pages.cs.wisc.edu/~remzi/OSTEP/file-journaling.pdf">42 - FSCK and Journaling</a>

And if you'd like to go further:

 * 44 - Data Integrity and Protection

 * 47 - Distributed Systems

 * 48 - NFS

