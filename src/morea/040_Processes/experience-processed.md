---
morea_id: experience_processes
morea_type: experience
title: "Homework Assignment #4"
published: True
morea_summary: "A programming assignment involving processes and IPC"
morea_labels: 
  - "Due Thu March 2nd 2017 23:55 HST"

---

## HW#4: Programming Assignment -- Processes and IPC

---

You are expected to do your own work on all homework assignments.  You
may (and are encouraged to) engage in <i>general</i> discussions with
your classmates regarding the assignments, but specific details of a
solution, including the solution itself, must always be your
own work (See the statement of Academic Dishonesty on the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>).

Check the 
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a> 
for the late assignment policy for this course.

<b>This assignement gives a total of 40 points and 10 extra-credits.</b>

The deadline is Thursday March 2nd 2017, 23:55 HST.

---

## What to turn in?

You should turn in an electronic tar gzipped archive named
<tt>ics332_hw4_UHUSERNAME.tar.gz</tt>. The archive MUST contain a
single top-level directory called ics332_hw4_UHUSERNAME, where
"UHUSERNAME" is your UH user name (e.g., for me, it would be
ics332_hw4_schastel). In that directory you should have all the
files <b>named exactly</b> as specified in the questions below.

Your report will be exclusively written in HTML and named
<tt>ics332_hw4_YourUHUserName/index.html</tt>. Your report must be
readable "as is", that is, without the reader having to perform any
operation. Points will be removed if the report is not readable.

Expected contents of the ics332_hw3_YourUHUserName directory:

 * index.html: the global report
 * ex1: the directory containing the source code for exercise 1,
   	with at least the file exercise1.c
 * ex2: the directory containing the source code for exercise 2,
   	with at least the file exercise2.c
 * ex3: the directory containing the source code for exercise 3,
   	with at least the file exercise3.c
 * ex4: the directory containing the source code for exercise 4,
   	with at least the file exercise4.c
 * ex5: the directory containing the source codes for exercise 5,
   	with possibly the files exercise51.c and exercise52.c

Additional contents can be provided if necessary in the relevant
directories and must be documented in the report. Any file present in
a directory which is not referenced in the global report will be ignored.

Your code must compile and execute "as delivered". You are also
required to deliver readable source code (commented, not obfuscated).

You are expected to deliver source code that pass all the tests. If
the tests do not pass, you can still provide the source (it has to
compile though) and explain why you think it does not work in your
report. That does not guarantee that you will get any point for the
question though.

---

## Environment

For this assignment you need a POSIX environment (see
<a href="{{ site.baseurl }}/morea/010_GettingStarted/experience-gettingstarted.html">HW#1</a>).

It is strongly recommended that you use "Good software practices",
e.g. code reuse, no copy/paste... except when explicitely
required. You are also strongly encouraged to "backup" your code using
git, mercurial, or any vcs/scm of your taste (Commit early, Commit often). 

Two functions (<tt>timestamp()</tt> and <tt>hotPotato()</tt>) 
are provided in this <a href="src/helpers.c">source code</a>

---

## Exercise 1  [10 points]

### Question 1.1 [7 points]

Write a C program named <tt>exercise1.c</tt> that takes exactly two
integer arguments (<i>sleep1</i> and <i>sleep2</i>) and performs the
following when executed:

 * The initial process creates exactly two other processes identified by
   <i>Child1</i> and <i>Child2</i>. The initial process will be
   identified as the <i>Parent</i> process;

 * When executed, the <i>Child1</i> process performs the following:
   * It displays: "<i>&lt;current time&gt;</i>: Child1 with pid <i>&lt;pid Child1&gt;</i>. Sleeping 
     for <i>&lt;sleep1&gt;</i> seconds"
   * It sleeps for about <i>&lt;sleep1&gt;</i> seconds
   * It displays: "<i>&lt;current time&gt;</i>: Child1 terminating"
   * It exits with status 1

 * When executed, the <i>Child2</i> process performs the following:
   * It displays "<i>&lt;current time&gt;</i>: Child2 with pid <i>&lt;pid Child2&gt;</i>. 
     Sleeping for <i>&lt;sleep2&gt;</i> seconds"
   * It sleeps for about <i>&lt;sleep2&gt;</i> seconds
   * It displays: "<i>&lt;current time&gt;</i>: Child2 terminating"
   * It exits with status 2

 * After the children creation, the <i>Parent</i> process does the following:
   * It displays the message: "<i>&lt;current time&gt;</i>: In the parent, Child1 pid is <i>&lt;pid Child1&gt;</i>"
   * It displays the message: "<i>&lt;current time&gt;</i>: In the parent, Child2 pid is <i>&lt;pid Child2&gt;</i>"
   * It runs an infinite loop performing the following:
     * It displays: "<i>&lt;current time&gt;</i>: In the parent (pid <i>&lt;pid parent&gt;</i>). Kill me when you have enough."
     * It sleeps for 5 seconds

Note: For your convenience the function timestamp() displaying the
current timestamp is provided.

Testing:

Test your program with the values 4 and 8. The output after 20 seconds
should be similar to:

<pre>
2017-02-18T19:14:48Z: In the parent, Child1 pid is 5066
2017-02-18T19:14:48Z: In the parent, Child2 pid is 5067
2017-02-18T19:14:48Z: In the parent (pid 5065). Kill me when you have enough
2017-02-18T19:14:48Z: Child2 with pid 5067. Sleeping for 8 seconds
2017-02-18T19:14:48Z: Child1 with pid 5066. Sleeping for 4 seconds
2017-02-18T19:14:52Z: Child1 terminating
2017-02-18T19:14:53Z: In the parent (pid 5065). Kill me when you have enough
2017-02-18T19:14:56Z: Child2 terminating
2017-02-18T19:14:58Z: In the parent (pid 5065). Kill me when you have enough
2017-02-18T19:15:03Z: In the parent (pid 5065). Kill me when you have enough
2017-02-18T19:15:08Z: In the parent (pid 5065). Kill me when you have enough
</pre>

You can stop the execution of the program by typing CTRL-C.

### Question 1.2 [3 points]

Show the output of your program when the values are 30 and 60 in the
report for the first 75 seconds of execution.

What are the process statuses (check the status with, e.g.,
<pre>
watch -n 1 ps f
</pre>
in another terminal):

 * after about 15 seconds of execution
 * after about 45 seconds of execution
 * after about 75 seconds of execution

Explain why Child1 and then Child2 become Zombies.

Give a command (on the CLI) that you would use to stop the execution
of the Parent process.

## Exercise 2 - Synchronous wait [10 points]

Feel free to reuse some of the code you wrote for the previous question.

### Question 2.1 [7 points]

Write a C program named exercise2.c such that:

 * The process creates exactly two other processes identified by
   <i>Child1</i> and <i>Child2</i>. The initial process will be
   identified as the <i>Parent</i> process;

 * When executed, the <i>Child1</i> process performs the following:
   * It displays: "<i>&lt;current time&gt;</i>: Child1 with pid <i>&lt;pid of Child1&gt;</i>. Sleeping for <i>&lt;sleep1&gt;</i> seconds"
   * It sleeps for about <i>&lt;sleep1&gt;</i> seconds
   * It displays: "<i>&lt;current time&gt;</i>: Child1 terminating"
   * It exits with exit status 1

 * When executed, the <i>Child2</i> process performs the following:
   * It displays "<i>&lt;current time&gt;</i>: Child2 with pid <i>&lt;pid of Child2&gt;</i>. Sleeping for <i>&lt;sleep2&gt;</i> seconds"
   * It sleeps for about <i>&lt;sleep2&gt;</i> seconds
   * It displays: "<i>&lt;current time&gt;</i>: Child2 terminating"
   * It exits with exit status 2

 * After the children creation, the <i>Parent</i> process does the following:
   * It displays the message: "<i>&lt;current time&gt;</i>: In the parent, Child1 pid is <i>&lt;pid Child1&gt;</i>"
   * It displays the message: "is <i>&lt;current time&gt;</i>: In the parent, Child2 pid is is <i>&lt;pid Child2&gt;</i>"
   * Wait for a child to terminate (ChildX, which is either Child1, or Child2)
   * Display "is <i>&lt;current time&gt;</i>: In the parent, ChildX has terminated (pid is <i>&lt;pid of childX&gt;</i>"
   * Wait for the other child to terminate (ChildY, which is either Child2, or Child1) 
   * Display "is <i>&lt;current time&gt;</i>: In the parent, ChildY has terminated (pid is <i>&lt;pid of childY&gt;</i>"
   * It then runs an infinite loop performing the following:
     * It displays: "is <i>&lt;current time&gt;</i>: In the parent (pid <i>&lt;pid parent&gt;</i>). Kill me when you have enough."
     * It sleeps for 5 seconds

Test your program with the values 8 and 4. The output after 20 seconds
should be similar to:

<pre>
2017-02-13T02:30:00Z: Child1 with pid 16413. Sleeping for 8 seconds
2017-02-13T02:30:00Z: In the parent, Child1 pid is 16413
2017-02-13T02:30:00Z: In the parent, Child2 pid is 16414
2017-02-13T02:30:00Z: Child2 with pid 16414. Sleeping for 4 seconds
2017-02-13T02:30:04Z: Child2 terminating
2017-02-13T02:30:04Z: In the parent, Child 2 has terminated (pid 16414)
2017-02-13T02:30:08Z: Child1 terminating
2017-02-13T02:30:08Z: In the parent, Child 1 has terminated (pid 16413)
2017-02-13T02:30:08Z: In the parent (pid 16412). Kill me when you have enough
2017-02-13T02:30:13Z: In the parent (pid 16412). Kill me when you have enough
2017-02-13T02:30:18Z: In the parent (pid 16412). Kill me when you have enough
</pre>

### Question 2.2 [3 points]

Show the output of your program when the values are 60 and 30 in your
report for the first 75 seconds of execution.

What are the process statuses:
 * at about 15 seconds
 * at about 45 seconds
 * at about 75 seconds

Explain why Child1 and then Child2 do not become Zombies.

## Exercise 3: Asynchronous wait [10 points]

Feel free to reuse some of the code you wrote for the previous questions.

### Question 3.1 [7 points]

Write a C program (exercise3.c) so that:

 * The process creates exactly two other processes identified by
   <i>Child1</i> and <i>Child2</i>. The initial process will be
   identified as the <i>Parent</i> process;

 * When executed, the <i>Child1</i> process performs the following:
   * It displays: "<i>&lt;current time&gt;</i>: Child1 with pid <i>&lt;pid of Child1&gt;</i>. Sleeping for <i>&lt;sleep1&gt;</i> seconds"
   * It sleeps for about <i>&lt;sleep1&gt;</i> seconds
   * It displays: "<i>&lt;current time&gt;</i>: Child1 terminating"
   * It exits with exit status 1

 * When executed, the <i>Child2</i> process performs the following:
   * It displays "<i>&lt;current time&gt;</i>: Child2 with pid <i>&lt;pid of Child2&gt;</i>. Sleeping for <i>&lt;sleep2&gt;</i> seconds"
   * It sleeps for about <i>&lt;sleep2&gt;</i> seconds
   * It displays: "<i>&lt;current time&gt;</i>: Child2 terminating"
   * It exits with exit status 2

 * After the children creation, the <i>Parent</i> process does the following:
   * It displays the message: "<i>&lt;current time&gt;</i>: In the parent, Child1 pid is <i>&lt;pid Child1&gt;</i>"
   * It displays the message: "<i>&lt;current time&gt;</i>: In the parent, Child2 pid is <i>&lt;pid Child2&gt;</i>"
   * While there is a child that is not terminated, it  runs the loop performing the following:
     * It displays: "<i>&lt;current time&gt;</i>: In the parent (pid <i>&lt;pid parent&gt;</i>), there are <i>&lt;a number&gt;</i> 
     children left. Sleeping for 1 second."
     * It sleeps for 1 second
   * When a child terminates (use the SIGCHLD signal and write the associated handler):
     * Display "<i>&lt;current time&gt;</i>: In the parent, a child has terminated (pid <i>&lt;pid of child&gt;</i>)"

Test your program with the values 8 and 4. The output after 20 seconds
could look like:
<pre>
2017-02-13T02:33:42Z: In the parent, Child1 pid is 16463
2017-02-13T02:33:42Z: In the parent, Child2 pid is 16464
2017-02-13T02:33:42Z: In the parent, there are 2 children left. Sleeping for 1 second
2017-02-13T02:33:42Z: Child1 with pid 16463. Sleeping for 8 seconds
2017-02-13T02:33:42Z: Child2 with pid 16464. Sleeping for 4 seconds
2017-02-13T02:33:43Z: In the parent, there are 2 children left. Sleeping for 1 second
2017-02-13T02:33:44Z: In the parent, there are 2 children left. Sleeping for 1 second
2017-02-13T02:33:45Z: In the parent, there are 2 children left. Sleeping for 1 second
2017-02-13T02:33:46Z: Child2 terminating
2017-02-13T02:33:46Z: In the parent, a child has terminated (pid 16464)
2017-02-13T02:33:46Z: In the parent, there are 1 children left. Sleeping for 1 second
2017-02-13T02:33:47Z: In the parent, there are 1 children left. Sleeping for 1 second
2017-02-13T02:33:48Z: In the parent, there are 1 children left. Sleeping for 1 second
2017-02-13T02:33:49Z: In the parent, there are 1 children left. Sleeping for 1 second
2017-02-13T02:33:50Z: Child1 terminating
2017-02-13T02:33:50Z: In the parent, a child has terminated (pid 16463)
2017-02-13T02:33:50Z: In the parent, no more children to wait for, exiting
</pre>

### Question 3.2 [3 points]

Show the output of your program when the values are 60 and 30 for the
first 75 seconds of execution.

## Exercise 4: The "Hot Potato" Game [10 points]

Feel free to reuse some of the code you wrote for the previous
questions.

The purpose of this exercise is to have two Child processes, both
direct children of a Parent process, playing a (computer) variant of
"hot potato". The C source code will be named exercise4.c.

The "hot potato" is symbolized by an integer which is going to be
communicated from one process to another through shared memory. The
value of the potato is read from and written to a dedicated shared
memory region.

When reading the potato value, the child process must verify that it
is the recipient of the potato. If the potato value is 0, the potato
is "hot" and the process exits (after displaying that it caught the
hot potato). If the potato value is not 0, it is updated using the
hotPotato() function (provided) and written to the shared memory
region. Values are read from memory each second. The Child1 process
knows that it has to write to the Child2 process (and vice-versa,
Child2 writes to Child1). A process will not send a message to itself
and ignore messages whom it is not the recipient.

The Parent process is in charge of the creation of the shared memory
region. It will wait two seconds before sending the initial potato:
the recipient and the potato value are arguments of the command
line. While the children are playing, the Parent process does not do
anything. When one child process terminates, the Parent process then
terminates the other child process, acknowledges that termination,
reports the winner (the process that did not exit first) and exits
after one minute. You must make sure that no zombie is left behind.

It is recommended to have informative messages displayed showing the
progress in a process.

You will assume that there is no error at runtime (all processes
fork() nicely...). You will not be penalized if you do not manage the
potential runtime errors.

### Question 4.1 [1 point]

Write the list of requirements for the parent process.

### Question 4.2 [1 point]

Write the list of requirements for a child.

### Question 4.3 [1 point]

Implement the "int readPotato(char* shmRegion, int thisChild)" function that
reads from the shared memory region, and returns, either (if this child
is the message recipient,) the potato value, or (if this child is not the
message recipient,) -1.

Implement the "writePotato(char* shmRegion, int recipientChild, int
potatoValue)" function that writes the potato value to the shared
memory region for the recipient child.

Note: Think "simple" for the messages format: Something written with
sprintf() should be read without problem by sscanf().

### Question 4.4 [7 points]

Implement all the requirements that you identified in 4.1 and 4.2. You
will clearly identified in your source code where a requirement is
satisfied.

Report the output of your program for the arguments:
 * 1 6
 * 2 3

## Exercise 5 (10 Extra-credits)

Feel free to reuse some of the code you wrote for the previous
questions.

### Question 5.1 (3 extra-credits):

Instead of having a loop regularly reading the shared memory region in
a child process, pause() the child process. Activate a paused process
when the SIGUSR1 signal is received and tell the Parent process that
the shared memory has been updated by having the child sending the
SIGUSR2 signal to it.

Your source code will be named exercise51.c

### Question 5.2 (7 extra-credits):

Generalize for any number of children between 2 and 8. When a
child is eliminated, the Parent process is in charge of identifying
the process that should be awaken.

Your source code will be named exercise52.c


