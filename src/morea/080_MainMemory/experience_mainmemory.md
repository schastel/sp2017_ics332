---
morea_id: experience_mainmemory
morea_type: experience
title: "Homework Assignment #7"
published: True
morea_summary: "A programming assignment about memory"
morea_labels: 
  - "Due Thu April 27th 2017 23:55 HST"

---

## HW#7: Hands-On/Pencil-and-Paper Assignment -- Main Memory

---

You are expected to do your own work on all homework assignments.  You
may (and are encouraged to) engage in <i>general</i> discussions with
your classmates regarding the assignments, but specific details of a
solution, including the solution itself, must always be your
own work (See the statement of Academic Dishonesty on the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>).

Check the 
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a> 
for the late assignment policy for this course.

<b>This assignement gives a total of 22 points and 2 extra-credits.</b>

The deadline is Thursday April 27th 2017, 23:55 HST.

---

## What to turn in?

You should turn in an electronic tar gzipped archive named
<tt>ics332_hw7_UHUSERNAME.tar.gz</tt>. The archive MUST contain a
single top-level directory called ics332_hw7_UHUSERNAME, where
"UHUSERNAME" is your UH user name (e.g., for me, it would be
ics332_hw7_schastel). In that directory you should have all the
files <b>named exactly</b> as specified in the questions below.

Your report will be exclusively written in HTML and named
<tt>ics332_hw7_YourUHUserName/index.html</tt>. Your report must be
readable "as is", that is, without the reader having to perform any
operation. Points will be removed if the report is not readable.

Expected contents of the ics332_hw7_YourUHUserName directory:

 * index.html: your report

---

## Environment

For this assignment you need a POSIX environment (see
<a href="{{ site.baseurl }}/morea/010_GettingStarted/experience-gettingstarted.html">HW#1</a>).

---

## Exercise 1: Hands-On - Common Bugs with Memory Management in C 

This exercise is a partial rewrite (and a slight extension) of the
Homework found in <a
href="http://pages.cs.wisc.edu/~remzi/OSTEP/vm-api.pdf">OSTEP /
Interlude: Memory API</a>.

This exercise gives 14 points and 2 extra-credits.

Note: Considering the simplicity of each program requirements, you
should try to write them yourself and compare it with my
implementation.

### Part 1: Illegal Dereferencing 

Note: The techniques for debugging and using valgrind that you will
learn in this part will be reused in the second part.

Download the <tt><a download href="hwdata/null.c">null.c</a></tt> program that
creates a pointer to an integer, sets it to NULL, and then tries to
dereference it.

#### Question 1.1 [1 Point] 

Compile <tt>null.c</tt> into an executable called <tt>null</tt>. What
happens when you run this program?

#### Question 1.2 [1 Point] 

Compile this program with symbol information included (with the -g
flag). Doing so let’s put more information into the executable,
enabling the debugger to access more useful information about variable
names and the like. Run the program under the debugger by typing

<pre>
gdb ./null
</pre>

and then, once gdb is running, typing run. What does gdb show you?

#### Question 1.3 [1 Point] 

Finally, use the valgrind tool on this program. Use the memcheck
tool that is a part of valgrind to analyze what happens. Run this by
typing in the following:

<pre>
valgrind --leak-check=full ./null
</pre>

What happens when you run this? Can you interpret the output from the
tool?

### Part 2: Memory issues in C 

#### Question 2.1 [3 points] 

Download the <a download href="hwdata/forgotten_free.c">forgotten_free.c</a> program that
allocates memory using malloc() but forgets to free it before exiting.

Note: Try to write the program yourself!

What happens when this program runs? Can you use gdb to find any
problems with it? How about valgrind (again with the --leak-check=full
flag)?

#### Question 2.2 [3 points] 

Download the <a download href="hwdata/malloc100.c">malloc100.c</a> program that creates
an array of integers called data of size 100 using malloc and then, sets
data[100] to zero.

What happens when you run this program? What happens when you run this
program using gdb? Using valgrind? Is the program correct?

#### Question 2.3 [3 points] 

Download the <a download href="hwdata/suspicious.c">suspicious.c</a> program that
allocates an array of integers (as above), frees them, and then tries
to print the value of one of the elements of the array.

Does the program run? What happens when you use gdb on it? What
happens when you use valgrind on it?

#### Question 2.4 [2 points] 

Download the <a download href="hwdata/double_free.c">double_free.c</a> program that frees
the same chunk of memory twice.

What happens? Do you need tools to find this type of problem?


#### Question 2.5 [2 extra-credits] 

Download the <a download href="hwdata/random_free.c">random_free.c</a> program that passes
a funny value to free (e.g., a pointer in the middle of the array you
allocated above).

What happens? Do you need tools to find this type of problem?

---

## Exercise 2: Contiguous Memory Allocation [8 points] 

Consider the jobs arrivals and the current memory space depicted below.

![Jobs arrivals and memory](hwdata/bp1.jpg){:class="img-responsive"}

For each of the shown allocations below, state whether it is “First
Fit”, “Best Fit”, “Worst Fit”, or “Other”. Detail your answer.

![Allocations](hwdata/bp2.jpg){:class="img-responsive"}
