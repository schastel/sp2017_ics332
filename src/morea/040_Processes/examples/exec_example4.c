#include <unistd.h>
#include <stdio.h>

int main(int argc, char **argv) {
  if (fork() == 0) {
    // Child
    char* const arguments[] = {"ls", "-l", "/tmp", NULL};
    execv("/bin/ls", arguments);
  } else {
    // Parent
    while (1);
  }
  return 0;
}

