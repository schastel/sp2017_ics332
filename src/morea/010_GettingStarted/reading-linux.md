---
morea_id: reading-linux
morea_type: reading
title: "The Linux command-line"
published: True
morea_summary: "A set of materials to help you getting starting with the Linux command-line in case it's needed"
morea_sort_order: 8
morea_labels: 
  - "Lecture notes"
  - "Tutorials"
  - "Games"
---

## Learning the Linux command line

In this course, some (very minimal) knowledge of the Linux command line will be
helpful. Being proficient
with the command line is a very valuable skill (for future courses, and in most
professional contexts). 

To help those of you with little or no experience here are some useful pointers:

  - A set of [lecture notes](linuxshell.pdf) (sort of random and unsorted!)
  - Learning the command-line at the [Code Academy](https://www.codecademy.com/courses/learn-the-command-line) (you will have to sign up for a free account)
  - The low-tech [Terminus Game](http://web.mit.edu/mprat/Public/web/Terminus/Web/main.html) to learn how to navigate the file system
  - A tutorial with tons of useful links  at [http://linuxcommand.org](http://linuxcommand.org)
  - A [comprehensive tutorial](http://ryanstutorials.net/linuxtutorial/)

