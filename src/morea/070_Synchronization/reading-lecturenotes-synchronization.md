---
morea_id: lecturenotes_synchronization
morea_type: reading
title: "Synchronization"
published: True
morea_summary: "Race conditions, locks, condition variables, monitors"
morea_url: /morea/070_Synchronization/synchronization.pdf
morea_labels: 
  - "Lecture notes"
---
