---
title: "Homework Assignment #1"
published: true
morea_id: experiences-gettingstarted
morea_summary: "An assignment to make sure you have access to
a (virtual) Linux box"
morea_type: experience
morea_sort_order: 0
morea_labels: 
- "Due Thu Jan 26th 2017, 23:55 HST"
---

## Hands-On Assignment: Accessing a Linux system

---
You are expected to do your own work on all homework assignments.  You
may (and are encouraged to) engage in <i>general</i> discussions with
your classmates regarding the assignments, but specific details of a
solution, including the solution itself, must always be your
own work (See the statement of Academic Dishonesty on the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>).

#### What to turn in? 

You should turn in an electronic tar gzipped archive named
<tt>ics332_hw1_UHUSERNAME.tar.gz</tt>. The archive MUST contain a
single top-level directory called ics332_hw1_UHUSERNAME, where
"UHUSERNAME" is your UH user name (e.g., for me, it would be
ics32_hw1_schastel). Inside that directory you should have all your
other files, <b>named exactly</b> as specified in the questions below.

Your report will be exclusively written in HTML (a template is
provided <a download href="ics332_hw1_YourUHUserName/index.html">here</a>).

If you really hate writing raw HTML, tools like https://html-online.com/editor could help.

---

This assignment gives a total of 9 points (Homeworks).

---

<h3>Install a Virtual Machine (6 points)</h3>

Assignments in this course require that you use a POSIX-compliant
system. To avoid differences between the expected results and the
results you'll be getting in them, we will ask you to install a
well-defined Linux OS on your computer.

<p/>
The purpose of this assignment is to install a Linux Virtual Machine
on your computer (please do it even if you already use Linux).

<p/>

Carefully follow the following steps:
<ul>
<li>(1.5GB to download) Download the 64-bit Ubuntu 16.04 desktop distribution, which you can get from
the <a
href="http://mirror.ancl.hawaii.edu/linux/ubuntu-releases/16.04.1/">UH
mirror</a> (ubuntu-16.04.1-desktop-amd64.iso) </li>

<li>Install the VirtualBox Virtual Machines Monitor (a.k.a. a hypervisor) (other VMM exist: VMWare,
Xen...). See the instructions at <a
href="http://henricasanova.github.io/VirtualBoxUbuntuHowTo.html">
Installing Ubuntu on VirtualBox</a> page. Make sure to complete all steps, that is:
<ul>
<li>Install Ubuntu (15-20 minutes)</li>
<li>Install VirtualBox additions (5 minutes)</li>
<li>Create a shared folder (1 minute)</li>
</ul></li>
<li>Reboot your VM and log in</li>
<li>In a terminal, execute <tt>cat /proc/cpuinfo | grep processor | wc -l</tt> (it is the number of cpus in your VM)</li>
<li>Shutdown your VM</li>
<li>Boost your VM by changing the number of processors (it's 1 by default): Settings / System / Processor. Use the maximal suggested value (2? 4? more? less?)</li>
<li><b>Question 1 (1 point)</b> Restart your VM and check its "new" number of cpus. Report that in the index.html file</li>

<li>Make a (first) clone of your Virtual Machine (Let's call it "Ubuntu Clone Almost Good" to fix ideas). If something (bad) happens in the next steps, that will avoid reinstalling everything from scratch</li>

<li>Softwares for the future
<ul>
<li>Install the open source Java 8</li>
<pre>
sudo apt-get update
sudo apt-get install default-jdk
</pre>
<li><b>Question 2.1 (1 point):</b> Report the default Java version with <tt>java -version</tt> or <tt>/usr/lib/jvm/java-8-openjdk-amd64/bin/java -version</tt>
<li>Install the Java 8 Oracle version</li>
<pre>
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer
</pre>
</li>
<li><b>Question 2.2 (1 point):</b> Report the Oracle Java version with <tt>java -version</tt> or <tt>/usr/lib/jvm/java-8-oracle/bin/java -version</tt></li>
<li><b>Question 2.3 (1 point):</b> Report the C compiler version <tt>gcc --version</tt></li>
</ul>

<li>Install the hwloc package (sudo apt-get install hwloc) and run
lstopo. Use <t>man lstopo</t> to know how to get the picture written
to the PNG image file names <tt>lstopo.png</tt>.

<p><b>Question 3 (2 points)</b> Provide the <tt>lstopo.png</tt> image in your report. Compare with the
results of Question 1</li>

<li>Make a clone (a full clone): Let's call it "Ubuntu Installation
Reference Clone". If during the semester, you have issues with your VM
you will have a fresh one without having to reinstall everything. Feel
free to delete the "Ubuntu Clone Almost Good" VM.</li>

</li>
</ul>

<i>Note: </i>From now on, you know the basics about VM
manipulation. Feel free to do your own backups at any time.

<h3>Reporting (3 points)</h3>

It is expected that your archive complies with the following requirements:

<p>
<b>Requirement 1 (1 point):</b> The name of your archive shall be: 
<pre>
ics332_hw1_UHUSERNAME.tar.gz
</pre>
where UHUSERNAME is your UH User Name.

<p>
<b>Requirement 2 (1 point):</b> When extracted your full report shall exactly contain the two files named:
<pre>
ics332_hw1_UHUSERNAME/index.html
ics332_hw1_UHUSERNAME/lstopo.png
</pre>

where UHUSERNAME is your UH User Name (as in 'UHUSERNAME@hawaii.edu')

<p>
<b>Requirement 3 (1 point):</b> There shall be no other file or
directory than those mentioned in Requirement 2 in the tar gzipped archive.

<p>
Notes:
<ul>
<li>Always make a backup of your data when you are not sure of what
you are doing (... and even when you think you know)!
</li>
<li>
To create a tarball gzipped archive: <tt>tar cvfz archive.tar.gz file1 file2 directory1 [...]</tt>
</li>
<li>
To verify a tarball gzipped archive: <tt>tar tvfz archive.tar.gz</tt>
</li>
<li>
To extract from a tarball gzipped archive: <tt>tar xvfz archive.tar.gz</tt>
</li>
</ul>

<p/>



