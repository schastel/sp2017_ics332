---
morea_id: 010_GettingStarted
morea_type: module
title: "Getting Started"
published: True
morea_coming_soon: False
morea_highlight: False
morea_outcomes: 
morea_readings: 
  - reading-lecturenotes_intro
  - reading-syllabus
  - reading-linux
  - reading-java
  - reading-screencast-guided-tour-website
morea_experiences: 
  - experiences-gettingstarted
morea_assessments: 
morea_sort_order: 10
morea_icon_url: /morea/010_GettingStarted/logo.jpg
---
