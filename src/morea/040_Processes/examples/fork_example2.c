#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
  pid_t pid;
  int A;

  A = 12;

  pid = fork();

  if (pid < 0) {
    fprintf(stderr,"Error: can't fork a process\n");
    perror("fork()");
    exit(1);
  } else if (pid) { // I am the parent
    sleep(10); // sleeping, giving time to the child to run
    fprintf(stdout,"I am the parent: A=%d\n",A);
    while(1);  // infinite loop
  } else {
    A += 3;
    fprintf(stdout,"I am the child and I've set A to %d\n",A);
    while (1) ; // infinite loop
  }
}

