---
morea_id: lecturenotes_processes
morea_type: reading
title: "Processes"
published: True
morea_summary: "Process, address space, process control block in the kernel, context switches, fork(), orphans, zombies"
morea_url: /morea/040_Processes/processes_handout.pdf
morea_labels: 
  - "Lecture notes"
---
