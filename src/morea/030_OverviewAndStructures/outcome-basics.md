---
morea_id: outcomebasics
morea_type: outcome
title: "Operating Systems"
published: True
morea_sort_order: 10
---

- Understand the role of and design options for Operating Systems, in the context of past and current such systems.


