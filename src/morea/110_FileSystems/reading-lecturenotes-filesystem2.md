---
morea_id: lecturenotes_filesystem2
morea_type: reading
title: "File System Implementation"
published: True
morea_summary: "File system data structures, disk block allocation, journaling"
morea_url: /morea/110_FileSystems/ics332_filesystem2.pdf
morea_labels: 
  - "Lecture notes"
---
