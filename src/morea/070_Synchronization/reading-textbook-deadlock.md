---
morea_id: textbook_conbugs
morea_type: reading
title: "Concurrency Bugs"
published: True
morea_labels: 
  - "Reading"
morea_summary: "Textbooks readings"
---

OSC: Sections 7.1-4, 7.5 (not 7.5.3), 7.6 (not 7.6.2), 7.7-8

OSTEP: [Chapter 32 - Common Concurrency Problems]{http://pages.cs.wisc.edu/~remzi/OSTEP/threads-bugs.pdf}
