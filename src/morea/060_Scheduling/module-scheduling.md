---
morea_id: 060_Scheduling
morea_type: module
title: "Scheduling"
published: True
morea_coming_soon: False
morea_highlight: False
morea_outcomes: 
  - outcomescheduling
morea_readings: 
  - lecturenotes_scheduling
  - ostep1_scheduling
  - ostep2_scheduling
  - ostep3_scheduling
morea_experiences: 
  - experience_scheduling
#  - experience_schedulingsimulation
morea_assessments: 
morea_sort_order: 70
morea_icon_url: /morea/060_Scheduling/logo.jpg
---
