---
morea_id: textbook_mainmemory
morea_type: reading
title: "Main Memory"
published: True
morea_labels: 
  - "Reading"
morea_summary: "Textbooks readings"
---


<a target="_blank" href="http://pages.cs.wisc.edu/~remzi/OSTEP/vm-api.pdf">Chapter 14 - Memory API</a>

<a target="_blank" href="http://pages.cs.wisc.edu/~remzi/OSTEP/vm-mechanism.pdf">Chapter 15 - Address Translation</a>

<a target="_blank" href="http://pages.cs.wisc.edu/~remzi/OSTEP/vm-segmentation.pdf">Chapter 16 - Segmentation</a>
