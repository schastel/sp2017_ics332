---
morea_id: overviewandstructures
morea_type: module
title: "OS Overview and Structures"
published: True
morea_coming_soon: False
morea_highlight: False
morea_outcomes: 
  - outcomebasics
morea_readings: 
  - lecturenotes_overview
  - lecturenotes_structures
  - textbook_overview
morea_experiences: 
  - experience_structure
morea_assessments: 
morea_sort_order: 30
morea_icon_url: /morea/030_OverviewAndStructures/logo.jpg
---

