---
morea_id: 110_FileSystem
morea_type: module
title: "File System"
published: True
morea_coming_soon: False
morea_highlight: False
morea_outcomes: 
  - outcomefilesystems
morea_readings: 
  - lecturenotes_filesystem1
  - lecturenotes_filesystem2
  - textbook_filesystem
morea_experiences: 
morea_assessments: 
morea_sort_order: 130
morea_icon_url: /morea/110_FileSystems/logo.jpg
---

