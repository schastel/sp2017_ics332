---
morea_id: reading-lecturenotes_intro
morea_type: reading
title: "Getting Started"
published: True
morea_summary: "Course introduction, logistics, administrivia"
morea_sort_order: 8
morea_url: /morea/010_GettingStarted/introduction.pdf
morea_labels: 
  - "Lecture notes"
---

[lecture notes](ics332_intro.pdf)
