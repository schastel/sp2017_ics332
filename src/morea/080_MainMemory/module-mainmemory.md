---
morea_id: 080_MainMemory
morea_type: module
title: "Main Memory"
published: True
morea_coming_soon: False
morea_highlight: False
morea_outcomes: 
  - outcomememory
morea_readings: 
#  - lecturenotes_countingaddressing
  - lecturenotes_mainmemory
  - textbook_mainmemory
  - src_mainmemory
morea_experiences: 
  - experience_mainmemory
morea_assessments: 
morea_sort_order: 100
morea_icon_url: /morea/080_MainMemory/logo.jpg
---

