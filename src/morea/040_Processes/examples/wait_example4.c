#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define CHILDREN 5

int childrenLeft = CHILDREN;

void acknowledge_child_termination(int signal) {
  if (signal == SIGCHLD) {
    // A child just terminated so this will be non-blocking
    int childReturnStatus;
    pid_t terminatedChildPID = wait(&childReturnStatus);
    printf("Child %d has completed with status %d\n", 
	   terminatedChildPID, WEXITSTATUS(childReturnStatus));
    childrenLeft--;
    return;
  } else {
    printf("How can I get there?n");
  }
}

int main(int argc, char **argv) {
  pid_t pidChildren[CHILDREN];
  int child;

  signal(SIGCHLD, acknowledge_child_termination);

  for (child = 0; child < CHILDREN; child++) {
    pidChildren[child] = fork();
    if (pidChildren[child] < 0) {
      fprintf(stderr,"Error: can't fork a process. Aborting\n");
      exit(-1);
    } else if (pidChildren[child] == 0) {
      srandom(getpid());//Don't bother: it's just to get different random numbers
      int jobDuration = 5 + (rand() % 8); // Job will last between 5 and 12 seconds
      printf("I'm child %d (%d) and I will work during %d seconds\n", 
	     child, getpid(), jobDuration);
      sleep(jobDuration);
      exit(child+100); // So that we are sure all ps have different exit status
    }
  }

  printf("All children started now\n");
  while (childrenLeft > 0) {
    printf("This is a message from the parent. I'm working.\n");
    sleep(1);
  }

  printf("This is a message from the parent. I should exit but you can check that I left no zombie.\n");
  while (1);
  exit(0);
}

