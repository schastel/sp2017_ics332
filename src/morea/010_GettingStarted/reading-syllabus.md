---
morea_id: reading-syllabus
morea_type: reading
title: "Syllabus"
published: True
morea_summary: "Detailed course syllabus"
morea_labels: 
  - "Syllabus"
---


## Course syllabus for ICS332

### Overview

This syllabus and an overall view of the course is further described
in [these slides](./ics332_intro.pdf).

In this class we will understand the role(s) of an operating system
and its design principles, discussing both Linux and Windows. We will
study the way in which an operating system manages running programs,
memory, inter-process/thread communication, and file systems. More
specifically, the ordered list of course topics is:

- Jan 10: Introduction. OS Overview
- Jan 17: OS structures
- Jan 24: Processes
- Jan 31: Inter-Process Communication
- Feb 7: Threads
- Feb 14: Scheduling
- Feb 21: Synchronization
- Feb 28: Deadlocks
- Mar 7: Review + <b>Mid-term</b>
- Mar 14: Main Memory
- Mar 21: Virtual Memory 
- Mar 28: [No class - Spring Recess]
- Apr 4: Virtual Memory
- Apr 11: Mass Storage
- Apr 18: File Systems
- Apr 25: <i>Buffer</i> (Virtual Machines?)
- May 2: <i>Buffer</i> + Review
- May 9: Review + <b>Final</b>

This course is hands-on and will involve a fair amount of programming,
in C, in Java, and maybe in Python.

---

### Student Outcomes (SOs)

* __Course-Specific SOs:__
  * Basic notions of computer architecture.
  * An understanding of the role of and design options for Operating
    Systems, in the context of past and current such systems.
  * An understanding of and hands-on experience with the process abstraction, including inter-process communication.
  * An understanding of and hands-on experience with the thread abstraction.
  * An understanding of synchronization issues (race conditions, locks, deadlocks). 
  * An understanding of the need for process/thread scheduling, and a knowledge of essential scheduling algorithms and strategies.
  * An understanding of the deadlock problem and of its possible solutions.
  * An understanding of the memory management problem and of possible solutions.
  * An understanding of the virtual memory abstraction as provided by current Operating Systems. 
  * An understanding of how Operating Systems manage storage and file systems.

* __Program SOs:__
  * <b>SO#1:</b> An ability to apply mathematical foundations, algorithmic principles, and computer science theory to the modeling and design of computer-based systems.
  * <b>SO#2:</b> An ability to define the computing requirements of a problem and to design appropriate solutions based on established design principles and with an understanding of the trade-offs involved in design choices.
  * <b>SO#3:</b> An ability to use sound development principles to implement computer-based and software systems of varying complexity, and to evaluate such systems.
  * <b>SO#8:</b> An ability to use current techniques, skills, and tools necessary for computing practice.

---

### Prerequisites

(ICS 311 or EE 367) and ICS 314 (Reference: <a
href="http://www.catalog.hawaii.edu/courses/departments/ics.htm">ICS website</a>)

---

### Instructor

Serge Chastel<br>
Office: POST-303G / Institute for Astronomy Mānoa B-134<br>
Office hours: Tuesday 5pm-6pm POST-303G or by appointment<br>
e-mail: schastel@hawaii.edu<br>

---

### Teaching Assistant

Serge Negrashov<br>
Office: POST-307<br>
Office hours: Mon 1-3pm<br>
e-mail: sin8@hawaii.edu<br>

---

### Course Website

<a href="http://schastel.github.io/sp2017_ics332" target="_blank">http://schastel.github.io/sp2017_ics332</a>

The Website is updated often and is the authoritative source for all course material.

---

### Lectures

Tue 6PM-8:30PM<br>
Room: POST-127

---

### Textbook

<a href="http://pages.cs.wisc.edu/~remzi/OSTEP/"><i>Operating Systems:
Three Easy Pieces</i></a> by Arpaci-Dusseau, R.H. and Arpaci-Dusseau,
A.C.

---

## Course Organization

  - Lectures: The lectures present the core of the material
  
  - Readings: Reading assignments provide preparation and a reference
    for the lectures. They are not substitutes for the lectures. At
    times the lecture may go further in depth than the textbooks, and
    at times the textbooks will go further in depth than the lecture,
    in which case the instructor and the Web site will give specific
    instructions as to which reading material is required.
  
  - Quizzes: There are announced quizzes in class throughout the
    semester. Only the <it>n-1</it> best out of <it>n</it> will be
    taken into account (e.g. your 7 best quizzes if we do 8).
  
  - Homework Assignments:
  
    - "Paper-and-Pencil" Assignments, with some questions taken from the textbooks. Their role is to reinforce the readings and lectures.
    
    - Programming Assignments, that are in C, in Java, maybe in Python.
    
  - Exams: There is one midterm exam and one comprehensive final exam. 


---

### Grading


- Quizzes: __10% of the overall grade__
- Homework Assignments: __50% of the overall grade__
- Midterm: __15% of the overall grade__
- Final: __25% of the overall grade__


Grading will be as follows:

  - __A+__: ≥ 98% 
  - __A__: ≥ 92% and < 98%
  - __A-__: ≥ 90% and < 92%
  - __B+__: ≥ 88% and < 90%  
  - __B__: ≥ 82% and < 88%
  - __B-__: ≥ 80% and < 82%
  - __C+__: ≥ 78% and < 80%
  - __C__: ≥ 65% and < 78%
  - __F__: < 65%

---

### Assignment policy

Note: It can happend that a Pencil-and-Paper assignment and a
Programming assignment happen at the same time.

####  What to turn in?

1) Turn in your <b>own</b> work. It is <b>okay to discuss</b> homework
with others, and in fact is encourage as it can lead to fruitful
discussions and discoveries, but <b>the work you turn in must always be
your own</b>.

2) Answers to paper-and-pencil assignments should always include how the
answer was derived, unless specified otherwise.

e.g.:

<table>
<tr>
  <td>
  Q (3 points): How many seeds in 3 apples if each apple has 7 seeds on average?
  </td>
</tr>
<tr>
  <td>
Answer 1 (bad): 22
  </td>
</tr>
<tr>
  <td>
<i>Reviewer: Where does that number come from? No time to figure it out: 0 point</i>
  </td>
</tr>
<tr>
  <td>
Answer 2 (wrong but ok): 3 apples * 7 seeds/apple = 7*3 = 22
  </td>
</tr>
<tr>
  <td>
<i>Reviewer: OK... The student got it but messed up the numbers. Fine. 2.5 points. </i>
  </td>
</tr>
<tr>
  <td>
Answer 3 (good): 3 apples * 7 seeds/apple = 7*3 = 21
  </td>
</tr>
<tr>
  <td>
<i>Reviewer: All good. 3 points</i>
  </td>
</tr>
</table>

3) What the assignment consists of in terms of contents will be
detailed for each assignment.

#### How to turn in?

All assignments are be turned in via [Laulima](https://laulima.hawaii.edu/)

#### When to turn in? 

Assignments are due at 11:55PM on the due day. Late work will be
accepted, with a 10% grade penalty for <24 hours of lateness and a 50%
grade penalty for <48 hours of lateness.

For instance, if the assignment is due on 3/10 and is turned in on
3/11 at 11AM, a 10% penalty if applied to the grade. If the assignment
is turned in on 3/12 at 2AM, then a 50% penalty is applied. Turning in
assignments more than 48h late will always result in a 0.

Unless 

#### Grading

Points will be deducted when the specifications provided in the
assignment are not met by what the students turn in, and/or when the
turned in programs are not sufficiently robust, as explained in the
first lecture of the semester.

---


### Academic Dishonesty

All occurrences of academic dishonesty, as defined below, will result in a
grade of 0 for the assignment or exam, and in a memo in your ICS department
file describing the incident (and will be done for all students involved).
Should there be more than one memo of this type in your file, the incident
will be referred to the Dean of Students. Disciplinary sanctions range from
a warning to expulsion from the university, as seen at:
[http://www.catalog.hawaii.edu/about-uh/campus-policies1.htm](http://www.catalog.hawaii.edu/about-uh/campus-policies1.htm)

See relevant excerpts below:

#### Academic Integrity

The integrity of a university depends upon academic honesty, which
consists of independent learning and research. Academic dishonesty
includes cheating and plagiarism. The following are examples of
violations of the Student Conduct Code that may result in suspension
or expulsion from UH Manoa.

#### Cheating

Cheating includes, but is not limited to, giving unauthorized help
during an examination, obtaining unauthorized information about an
examination before it is administered, using inappropriate sources of
information during an examination, altering the record of any grade,
altering an answer after an examination has been submitted, falsifying
any official UH Manoa record, and misrepresenting the facts in order
to obtain exemptions from course requirements.

#### Plagiarism

Plagiarism includes, but is not limited to, submitting, to satisfy an
academic requirement, any document that has been copied in whole or in
part from another individual's work without identifying that
individual; neglecting to identify as a quotation a documented idea
that has not been assimilated into the student's language and style;
paraphrasing a passage so closely that the reader is misled as to the
source; submitting the same written or oral material in more than one
course without obtaining authorization from the instructors involved;
and "dry-labbing," which includes obtaining and using experimental
data from other students without the express consent of the
instructor, utilizing experimental data and laboratory write-ups from
other sections of the course or from previous terms, and fabricating
data to fit the expected results.

#### Disciplinary Action

The faculty member must notify the student of the alleged academic
misconduct and discuss the incident in question. The faculty member
may take academic action against the student as the faculty member
deems appropriate. These actions may be appealed through the Academic
Grievance Procedure, available in the Office of Judicial Affairs. In
instances in which the faculty member believes that additional action
(i.e., disciplinary sanctions and a UH Manoa record) should be
established, the case should be forwarded to the Office of Judicial
Affairs.
