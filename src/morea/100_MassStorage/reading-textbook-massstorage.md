---
morea_id: textbook_massstorage
morea_type: reading
title: "Textbook Reading"
published: True
morea_summary: "OSTEP 36-38 (+47?)"
morea_labels: 
  - "Reading"
---

 * <a href="http://pages.cs.wisc.edu/~remzi/OSTEP/file-devices.pdf">36 - I/O Devices</a>

 * <a href="http://pages.cs.wisc.edu/~remzi/OSTEP/file-disks.pdf">37 - HDD</a>

 * <a href="http://pages.cs.wisc.edu/~remzi/OSTEP/file-raid.pdf"> 38 - RAID</a>

 * <a href="http://pages.cs.wisc.edu/~remzi/OSTEP/file-ssd.pdf">Flash-based SSDs</a>

Non-required reading: 

If you want to know a little bit more about how the "cloud" works, have a look at
 <a href="http://pages.cs.wisc.edu/~remzi/OSTEP/dist-intro.pdf">47 - Distributed Systems</a> 
(note that it is in the "Persistence" part but does not really require knowledge about I/O except
networking concept).

