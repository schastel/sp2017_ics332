#define _GNU_SOURCE
#include <time.h>
#include <dlfcn.h>
#include <stdio.h>
 
struct tm *(*orig_localtime)(const time_t *timep);
 
struct tm *localtime(const time_t *timep) {
  time_t t = *timep - 20000;
  return orig_localtime(&t);
}
 
void
_init(void) {
  printf("Loading a weird date.\n");
  orig_localtime = dlsym(RTLD_NEXT, "localtime");
}
