---
morea_id: lecturenotes_filesystem1
morea_type: reading
title: "File System Interface"
published: True
morea_summary: "Files, directories, links, file tables"
morea_url: /morea/110_FileSystems/ics332_filesystem1.pdf
morea_labels: 
  - "Lecture notes"
---
