---
morea_id: outcomeprocesses
morea_type: outcome
title: "Processes"
published: True
morea_sort_order: 20
---

- Understand the process abstraction and how the OS implements it
- Understand process creation in Linux and gain hands-on experience with process creation in Java
- Understand popular Inter-Process-Communication (IPC) abstractions
- Gain hands-on experience with the Pipe abstraction


