---
morea_id: lecturenotes_ipcs
morea_type: reading
title: "IPCs"
published: True
morea_summary: "Message passing, RPC/RMI, Pipes, Signals"
morea_url: /morea/040_Processes/ipc_handout.pdf
morea_labels: 
  - "Lecture notes"
---
