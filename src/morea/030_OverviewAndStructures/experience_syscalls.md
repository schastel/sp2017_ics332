---
morea_id: experience_structure
morea_type: experience
title: "Homework Assignment #3"
published: True
morea_summary: "An assignment in which you use strace to count system calls and spy on programs"
morea_labels: 
  - "Due Thu Feb 16th 2017 23:55 HST"

---

## HW#3: Hands-On Assignment -- System Calls

---

You are expected to do your own work on all homework assignments.  You
may (and are encouraged to) engage in <i>general</i> discussions with
your classmates regarding the assignments, but specific details of a
solution, including the solution itself, must always be your
own work (See the statement of Academic Dishonesty on the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>).

Check the 
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a> 
for the late assignment policy for this course.

<b>This assignement gives a total of 18 points and 2 extra-credits.</b>

The deadline is Thursday February 16th 2017, 23:55 HST.

#### What to turn in? 

You should turn in an electronic tar gzipped archive named
<tt>ics332_hw3_UHUSERNAME.tar.gz</tt>. The archive MUST contain a
single top-level directory called ics332_hw3_UHUSERNAME, where
"UHUSERNAME" is your UH user name (e.g., for me, it would be
ics332_hw3_schastel). In that directory you should have all the
files <b>named exactly</b> as specified in the questions below.

Your report will be exclusively written in HTML and named
<tt>ics332_hw3_YourUHUserName/index.html</tt>. Your report must be
readable "as is", that is, without the reader having to perform any
operation. Points will be removed if the report is not readable.

Expected contents of the ics332_hw3_YourUHUserName directory:

 * index.html: your report

Additional contents can be provided if necessary.

---

#### Environment

For this assignment you need a Linux Virtual Machine
(see <a href="{{ site.baseurl }}/morea/010_GettingStarted/experiences-gettingstarted.html">Assignment #1</a>). 

---

<h2>strace'ing "Hello World!" [6 pts]</h2>

In this exercise you will use <tt><b>strace</b></tt> to investigate
the system calls placed when running HelloWorld programs in C, in
Java, and in bash. Each of them has a unique purpose, display:
<pre>
    Hello, World! It's &lt;current time&gt;!
</pre>

The source code of each program can be found in:

 * [HelloWorld.c](HW/HelloWorld.c)
 * [HelloWorld.java](HW/HelloWorld.java)
 * [HelloWorld.sh](HW/HelloWorld.sh)

<h3>C implementation</h3>

Create an executable (named HelloWorld) by running:
<pre>
gcc -o HelloWorld HelloWorld.c
</pre>

and make sure you can execute it:

<pre>
./HelloWorld
</pre>

<h4>Question 1.1 (2 points)</h4>

Using <tt>strace</tt>, generate the statistics about the system calls
placed when the C implementation of HelloWorld is executed.

Include the output of <tt>strace</tt> in your report (e.g. between
&lt;pre&gt; ... &lt;/pre&gt; HTML delimiters).

<h3>Java implementation</h3>

You will compile the source code by running:

<pre>
javac HelloWorld.java
</pre>

and can execute it with:

<pre>
java HelloWorld
</pre>

<h4>Question 1.2 (2 points)</h4>
Using <tt>strace</tt>, generate the statistics about the system calls
placed when the Java implementation of HelloWorld is executed.

Include the output of <tt>strace</tt> in your report (e.g. between
&lt;pre&gt; ... &lt;/pre&gt; HTML delimiters).

<h3>Bash implementation</h3>

Just execute the script by running:

<pre>
bash HelloWorld.sh
</pre>

<h4>Question 1.3 (2 points)</h4>
Using <tt>strace</tt>, generate the statistics about the system calls
placed when the bash implementation of HelloWorld is executed.

Include the output of <tt>strace</tt> in your report (e.g. between
&lt;pre&gt; ... &lt;/pre&gt; HTML delimiters).

<h2>System Calls Analysis (10 points)</h2>

<h4>Question 2.1 (4 points)</h4>
Pick 2 system calls common to all three implementations from the list
of system calls that you found in the previous section.

Briefly describe what they do and tell to which of these categories
you would attach them to:

 * Process Management
 * Memory Management
 * I/O Management
 * Protection/Security
 * Other (and in the latter case, detail why in your opinion they do
not fit in the first four categories)

Note: Multi-category is possible

Hint: <tt>man</tt> pages are your friends but make sure that you read
the correct one (e.g. <tt>man 2 write</tt> and not <tt>man
write</tt>).

Side note: Take this question at the end of the semester: Measure how
much your knowledge about OS has progressed <tt>:)</tt>

<h4>Question 2.2 (2 points)</h4>

If any, pick a system call which is shown while running the C implementation
but not in the two others.

Briefly describe it and tell to which category you would attach it.

<h4>Question 2.3 (2 points)</h4>

If any, pick a system call which is shown while running the Java implementation
but not in the two others.

Briefly describe it and tell to which category you would attach it.

<h4>Question 2.4 (2 points)</h4>

If any, pick a system call which is shown while running the bash implementation
but not in the two others.

Briefly describe it and tell to which category you would attach it.

<h2>Privileged Execution (2 Points + 2 Extra-credits)</h2>

In a terminal (try to) execute <tt>date -s 20:00</tt>.

<h4>Question 3.1 (2 points)</h4>

Explain what should happen assuming on a system without privileged
system calls, and what actually happens in your Linux box. Detail
which system call is the "privileged" one (use strace to identify it).

<h4>Question 3.2 (2 Extra-credits)</h4>

Use <tt>sudo</tt> to acquire the privilege of changing the system
clock. If you try to use <tt>strace</tt> to monitor what <tt>sudo</tt>
does, you will see that it fails. Investigate why (e.g. on the
Internet, but do not spend too much time on it).



