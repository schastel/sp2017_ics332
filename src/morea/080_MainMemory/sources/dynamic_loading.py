def SQRT(x):
    try:
        print "  Trying to execute function _sqrt()"        
        return _sqrt(x)
    except NameError:
        print "  Exception caught: Importing math"
        global _sqrt # Make _sqrt a global entity
        from math import sqrt as _sqrt # Load sqrt from the math module (rename it _sqrt)
        return SQRT(x)
    pass

if __name__ == "__main__":
    import sys
    a = float(sys.argv[1])
    print "Attempt 1"
    print SQRT(a)
    print "Attempt 2"
    print SQRT(a)
