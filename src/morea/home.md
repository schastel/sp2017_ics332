---
published: true
title: "Home"
morea_id: home
morea_type: home
---


### ICS 332 Spring 2017: Operating Systems

#### Lectures: POST-127, Tue, 6PM-8:30PM

#### Instructor: Serge Chastel  (<a href="mailto:schastel@hawaii.edu">email</a>)

#### Teaching Assistant: Serge Negrashov (<a href="mailto:sin8@hawaii.edu">email</a>)

#### Office hours: 
  - Instructor: Tue 5PM-6PM POST-303G (956-9564 if the doors to access it are closed) and by appointment (Institute for Astronomy - B134)
  - Teaching Assistant: Mon 1-3pm POST-307

---


#### Announcements:

<div style="padding:5px;" markdown="1" class="{% cycle 'section-background-1', 'section-background-2' %}">
Welcome to ICS332:
Use the top navigation bar for up-to-date course content as organized by
[Modules]({{ site.baseurl }}/modules/). These modules are interconnected
and will be covered in sequence throughout the semester. 
__All details about this course are in the [syllabus]({{ site.baseurl}}/morea/010_GettingStarted/reading-syllabus.html).__
</div>


<br>

