---
title: "Guided tour of Morea course websites"
published: true
morea_id: reading-screencast-guided-tour-website
morea_url: https://www.youtube.com/watch?v=7iQZrb7GEGc
morea_summary: "About Morea: The framework used to set up this website"
morea_type: reading
morea_sort_order: 0
morea_labels:
 - Screencast
 - 4 min
---

