#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define CHILDREN 5

int main(int argc, char **argv) {
  pid_t pidChildren[CHILDREN];
  int child;

  for (child = 0; child < CHILDREN; child++) {
    pidChildren[child] = fork();
    if (pidChildren[child] < 0) {
      fprintf(stderr,"Error: can't fork a process. Aborting\n");
      exit(-1);
    } else if (pidChildren[child] == 0) {
      int jobDuration = 5 + 2*child; //5, 7, ...
      printf("I'm child %d (%d) and I will work during %d seconds\n", 
	     child, getpid(), jobDuration);
      sleep(jobDuration);
      exit(child+100); // So that we are sure all ps have different exit status
    }
  }

  // If this part of the code is reached we are necessarilty in the
  // parent (you have to understand why)
  int childReturnStatus;
  pid_t terminatedChildPID;
  for (child = CHILDREN-1; child >= 0; child--) {
    printf("Waiting for completion of child: %d (%d)\n", child, pidChildren[child]);
    terminatedChildPID = waitpid(pidChildren[child], &childReturnStatus, 0);
    printf("Child %d has completed with status %d\n", 
	   terminatedChildPID, WEXITSTATUS(childReturnStatus));
  }
  exit(0);
}

