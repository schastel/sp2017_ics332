---
morea_id: experience_virtualmemorysimulation
morea_type: experience
title: "Homework Assignment #8"
published: True
morea_summary: "An assignment in which you simulate Virtual Memory"
morea_labels: 
  - "Due Sat May 6th, 2017 23:55"
---

# Programming Assignment #8 -- Virtual Memory Simulation

---

You are expected to do your own work on all homework assignments.  You
may (and are encouraged to) engage in <i>general</i> discussions with
your classmates regarding the assignments, but specific details of a
solution, including the solution itself, must always be your own
work. (See the statement of Academic Dishonesty on the <a
href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>.)

## What to turn in? 

You should turn in an electronic tar gzipped archive named
<tt>ics332_hw8_UHUSERNAME.tar.gz</tt>. The archive MUST contain a
single top-level directory called ics332_hw8_UHUSERNAME, where
"UHUSERNAME" is your UH user name (e.g., for me, it would be
ics332_hw8_schastel). In that directory you should have all the
files <b>named exactly</b> as specified in the questions below.

Your report will be exclusively written in HTML and named
<tt>ics332_hw8_YourUHUserName/index.html</tt>. Your report must be
readable "as is", that is, without the reader having to perform any
operation. Points will be removed if the report is not readable.

Expected contents of the ics332_hw8_YourUHUserName directory:

 * index.html: your report
 * src: the source code for the various questions, namely
   * src/edu/hawaii/ics332/mtuimpl/VirtualMemoryManagerV0.java: The source code
     of the class answering question 1;
   * src/edu/hawaii/ics332/mtuimpl/VirtualMemoryManagerV1.java: The source code
     of the class answering question 2;
   * src/edu/hawaii/ics332/mtuimpl/VirtualMemoryManagerV2.java: The source code
     of the class answering question 3;
   * src/edu/hawaii/ics332/mtuimpl/VirtualMemoryManagerV3.java: The source code
     of the class answering question 4;
   * src/edu/hawaii/ics332/mtuimpl/VirtualMemoryManagerV4.java: The source code
     of the class answering question 5 (extra-credit);
 * Any other file that you would think useful

The deadline for this homeowrk is Thursday, May 4th 2017, 23:55 HST.

The total number of points for this assignment is 48 points and 12
extra-credits.

---

## Environment

This assignment is a Java programming assignment and is platform independent.

See <a href="{{ site.baseurl }}/morea/010_GettingStarted/java.html">this help</a> if you are not familiar with Java (especially the "Clean way + dependencies on jar" part).

## Downloads

All downloads are here:

 * <a download href="hw/vmsimulation-2017050401.jar">vmsimulation-2017050401.jar</a>

 * <a download href="hw/scenarios_and_references-2017050502.tgz">scenarios_and_references-2017050502.tgz</a>

 * <a download href="hw/VirtualMemoryManagerV0.tgz">template</a>

## Assignment Overview

In this programming assignment, you will implement a simulation of a
Virtual Memory Management system. We consider a single process (i.e.,
there is one address space, one (non-inverted) page table, no context
switching).

A Java package named <a download
href="hw/vmsimulation-2017050401.jar">vmsimulation-2017050401.jar is provided to you
(download that file)</a>, and you will make a series of additions to
it. The package contains a few classes that are already implemented
and that you will not modify. Its associated Javadoc can be browsed from
<a href="hw/javadoc/index.html">here</a>. It contains:

 * <b>SystemSimulator</b>: a tester for the virtual memory system,
which uses dynamic class loading to test your code. It implements the
main method. You will call it with:

<pre>
java -cp vmsimulation-2017050401.jar edu.hawaii.ics332.computer.SystemSimulator version simulation_file
</pre>

where:
   * <i>version</i> is the implementation version, i.e. V0 (Question #1), V1 (Question #2), V2 (Question #3), V3 (Question #4), or V4 (Question #5);
   * <i>simulation file</i> is the name of the file describing the simulation to perform

 * <b>MainMemory</b>: an abstraction of the main memory.  See the <a
   href="./hw/javadoc/index.html">javadoc</a>.

 * <b>BackingStore</b>: an abstraction of the process address space on
    the backing store (i.e., the disk).  See the <a
    href="./hw/javadoc/index.html">javadoc</a>.

 * <b>Simulation</b>: A class related to the definition of
   simulations. The associated package related to the events happening
   in a simulation is located in the edu.hawaii.ics332.scenario
   package. Have a look at the implementation of the Scenario class and
   in particular of its create() method if you want to understand how
   a series of event (i.e. a Scenario) is built.

   A Simulation is a set of parameters defining the context of the simulation, namely:
   
   * memorySize: the memory size of the computer system (in bytes)

   * diskSize: the disk size of the backing store (in bytes)
   
   * pageSize: the page size (in bytes)
   
   * rngSeed: a seed to initialize the random number generator
   
   * locality: an integer, the locality telling how the random addresses are generated
   * out: the name of the file where the messages that you will log will be written
   * events: a sequence of events (see the Scenario class documentation)
   * Comment lines (that are not always acurate) are lines starting with a '#'

   e.g.:
<pre>
memorySize: 16
diskSize: 16
pageSize: 2
rngSeed: 1
locality: 0
out: out/v0-01.log
#
# Zero-memory
# Dump memory
# 5 times:
#   Store Random address, Random value
#   Load Random address
# Dump memory
#
events: Z; DM; {5}{SR,R; LR;}; DM;
</pre>

 * Some convenience classes, for Exceptions in the edu.hawaii.ics332.exceptions
   package and for logging in the edu.hawaii.ics332.logging package, for randomization

When <tt>java -cp vmsimulation-2017050401.jar
edu.hawaii.ics332.computer.SystemSimulator VERSION
simulation_file</tt> is executed, the byte code of the class named
<tt>edu.hawaii.ics332.mtuimpl.VirtualMemoryManagerVERSION</tt> is dynamically
loaded and the simulation defined in <tt>simulation_file</tt> creates a simulation instance.

Once the class byte code has been loaded a VirtualMemoryManagerVERSION
is instanciated. A VirtualMemoryManagerVERSION must extend the
abstract class MemoryTranslationUnit and its constructor must be of
the form:

<pre>
public VirtualMemoryManagerVERSION(Integer memorySize, 
			Integer diskSize, 
			Integer pageSize,
			Integer locality,
			String out) throws ICS332MemoryException, ICS332IOException;
</pre>

For your convenience you will find a skeleton for
VirtualMemoryManagerV0 in the edu.hawaii.ics332.mtuimpl package.

The objective of this assignment is for you to implement VirtualMemoryManagerV0,
VirtualMemoryManagerV1, VirtualMemoryManagerV2, and
VirtualMemoryManagerV3.

Once you have succesfully implemented VirtualMemoryManagerV0, you will
copy its source code as a base for the VirtualMemoryManagerV1
class. Once VirtualMemoryManagerV1 is implemented, you will copy its
source code as a base for VirtualMemoryManagerV2. And finally, once
VirtualMemoryManagerV2 is implemented, you will copy its source code
as a base for VirtualMemoryManagerV3.

You are strongly encouraged to use any VCS (Git, Mercurial, SVN...)
to keep track of your progress and make sure you don't overwrite any
file by mistake: commit often!

### Question #1 [12 points]: Version V0 - Memory only

In this question, your (simulated) system uses only the main memory,
addressing it via physical addresses (there is no address translation
whatsoever). Consequently, your virtual memory manager does not need
to do anything with the backing store at all.

You must implement the <b>writeByte()</b>, <b>readByte()</b>,
<b>zeroMemory()</b>, and <b>dumpMemoryContent()</b> methods in
<b>VirtualMemoryManagerV0.java</b>. A <a
href="hw/VirtualMemoryManagerV0.tgz">template is provided here</a>,
feel free to use it!

See the description of the methods in the <a
href="./hw/javadoc/index.html">javadoc</a>.) All four methods print
some output to the file defined by "out:" in the simulation file using
the <tt>MemoryTranslationUnit::log()</tt> method. You must use that
log() method to produce an output which will be compared to the reference.

For instance, when running:
<pre>
java -cp vmsimulation-2017050401.jar edu.hawaii.ics332.computer.SystemSimulator V0 scenarios/V0/01.vmem
</pre>

the output log is:
<pre>
1: Zeroing memory
2: Memory dump for VirtualMemoryManagerV0
0000: 0
0001: 0
0002: 0
0003: 0
0004: 0
0005: 0
0006: 0
0007: 0
0008: 0
0009: 0
000a: 0
000b: 0
000c: 0
000d: 0
000e: 0
000f: 0

3: Write -16 at 0002
4: Read -16 at 0002
5: Read -16 at 0002
6: Write -127 at 0000
7: Read -127 at 0000
8: Read -127 at 0000
9: Write 2 at 0000
10: Read 2 at 0000
11: Read 2 at 0000
12: Write -72 at 0000
13: Read -72 at 0000
14: Read -72 at 0000
15: Write 79 at 0004
16: Read 79 at 0004
5: Read -16 at 0002
6: Write -127 at 0000
7: Read -127 at 0000
8: Read -127 at 0000
9: Write 2 at 0000
10: Read 2 at 0000
11: Read 2 at 0000
12: Write -72 at 0000
13: Read -72 at 0000
14: Read -72 at 0000
15: Write 79 at 0004
16: Read 79 at 0004
17: Read 79 at 0004
18: Memory dump for VirtualMemoryManagerV0
0000: -72
0001: 0
0002: -16
0003: 0
0004: 79
0005: 0
0006: 0
0007: 0
0008: 0
0009: 0
000a: 0
000b: 0
000c: 0
000d: 0
000e: 0
000f: 0

19: Page faults: 0
20: Bytes transferred: 0
</pre>

The scenarios and the corresponding reference logfiles that you have
to run for the V0 version are in the archive that you can <a
href="hw/scenarios_and_references-2017050502.tgz">download from here</a>.

We will consider that there is no difference between your output
logfile and the corresponding reference logfile if the "diff -wq" tool
will return nothing (if there is a difference, you will see a message
like, "Files a and b differ"). As usual, "man diff" if you need the
details about the diff tool.

If you want to run all tests for V0, you can execute:
<pre>
java -cp hw/vmsimulation-2017050401.jar edu.hawaii.ics332.computer.tests.V0Test 
</pre>

### Question #2 [12 points]: Version V1 - Simple demand paging when memory size = disk size

In this and all following questions the virtual memory manager
now handles the disk,  meaning that all methods of
<b>VirtualMemoryManager</b> must be implemented in
<b>VirtualMemoryManagerV1</b>. (Again, see the <a
href="./hw/javadoc/index.html">javadoc</a>).

You will copy the V0 implemenation and name it V1 and start from that
V1 version.

Your virtual memory manager must implement demand paging.  In this
question <i>we assume that the memory size is equal to or larger than the
disk size</i>. This assumption, albeit unrealistic, simplifies the
operation of the virtual memory manager because it never runs out of
free memory frames as the entire address space can reside in
main memory if need be.

Unlike in the previous question, your virtual memory manager must perform address
translation (from virtual to physical), using a page table, following the steps that
we have described in lectures: 

  1. Extract the virtual page number and the offset from the virtual address;
  2. Look up the page table to see if the page is loaded in a frame;
  3. If it is not, then load it in a frame and update the page table;
  4. Translate the virtual page number into a physical frame number and construct the physical address;
  5. Access the data at that physical address.

You can implement the page table any way you see fit, but a
separate PageTable class with update(), lookup(), isValid() and other
methods that correspond to the use of the simplest page table scheme explained in the
lectures is likely best.

Your code, i.e., the methods implemented in <b>VirtualMemoryManagerV1.java</b>, 
in addition to printing out memory read/write operations
as in the previous question, must <tt>log()</tt> out which pages
are brought into which frames, count page faults, and count how many bytes
are transferred back and forth between the disk and the main memory.

For instance, this is the output to expect when running:
<pre>
$ java -cp bin edu.hawaii.ics332.computer.SystemSimulator V1 scenarios/V1/02.vmem
1: Disk dump:
PAGE 0: -10,-55,45,-93
PAGE 1: 58,-16,29,79
PAGE 2: -73,112,-23,-116
PAGE 3: 3,37,-12,29

2: Zeroing memory
3: Memory dump for VirtualMemoryManagerV1
0000: 0
0001: 0
0002: 0
0003: 0
0004: 0
0005: 0
0006: 0
0007: 0
0008: 0
0009: 0
000a: 0
000b: 0
000c: 0
000d: 0
000e: 0
000f: 0

4: Bringing page 1 into frame 0
5: RAM: 0011 <-- 109
6: Page 1 is in memory
7: RAM: 0011 --> 109
8: Page 1 is in memory
9: RAM: 0011 --> 109
10: Page 1 is in memory
11: RAM: 0011 <-- 35
12: Page 1 is in memory
13: RAM: 0011 --> 35
14: Page 1 is in memory
15: RAM: 0011 --> 35
16: Page 1 is in memory
17: RAM: 0011 <-- 88
18: Page 1 is in memory
19: RAM: 0011 --> 88
20: Page 1 is in memory
21: RAM: 0011 --> 88
22: Page 1 is in memory
23: RAM: 0011 <-- -14
24: Page 1 is in memory
25: RAM: 0011 --> -14
26: Page 1 is in memory
27: RAM: 0011 --> -14
28: Page 1 is in memory
29: RAM: 0011 <-- 112
30: Page 1 is in memory
31: RAM: 0011 --> 112
32: Page 1 is in memory
33: RAM: 0011 --> 112
34: Memory dump for VirtualMemoryManagerV1
0000: 58
0001: -16
0002: 29
0003: 112
0004: 0
0005: 0
0006: 0
0007: 0
0008: 0
0009: 0
000a: 0
000b: 0
000c: 0
000d: 0
000e: 0
000f: 0

35: writeBackAllPagesToDisk()
36: Disk dump:
PAGE 0: -10,-55,45,-93
PAGE 1: 58,-16,29,112
PAGE 2: -73,112,-23,-116
PAGE 3: 3,37,-12,29

37: Page faults: 1
38: Bytes transferred: 8
</pre>

We will consider that there is no difference between your output
logfile and the corresponding reference logfile if the "diff -wq" tool
will return nothing.

If you want to run all tests for V1, you can execute:
<pre>
java -cp hw/vmsimulation-2017050401.jar edu.hawaii.ics332.computer.tests.V1Test 
</pre>

###  Question #3 [12 points]: Version V2 - Demand paging with FIFO page replacement

In this question you enhance your virtual memory manager, now called
<b>VirtualMemoryManagerV2</b> (feel free to build up from
VirtualMemoryManagerV1), so that it can handle an address space that
is larger than the main memory. Essentially, it:

  - Keeps a list of free frames;
  - Evicts a victim page whenever a new page must be brought in and there are insufficient free frames;
  - Always selects the victim as the page that was <b>brought</b> into memory the longest time ago (your page table entries could keep track of some notion of time, or you could keep a FIFO data structure of page numbers that is separate from the page table);
  - A victim page is <b>always</b> written back to disk.

As in the previous question, your code must generate some log output: messages
about pages being evicted back to disk when they are selected as victims.
Sample execution can be found in the relevant output reference logfiles.

You will have to add messages like (e.g. in references/v2-07.log):
<pre>
[...]
11: RAM: 110 --> 44
12: Evicting page 0
13: Bringing page 5 into frame 0
14: RAM: 000 --> -35
15: Page 5 is in memory
[...]
</pre>

We will consider that there is no difference between your output
logfile and the corresponding reference logfile if the "diff -wq" tool
will return nothing.

If you want to run all tests for V2, you can execute:
<pre>
java -cp hw/vmsimulation-2017050401.jar edu.hawaii.ics332.computer.tests.V2Test 
</pre>

### Question #4 [12 points]: Version V3 - Adding a dirty bit

In this question you enhance your virtual memory manager, now called
<b>VirtualMemoryManagerV3</b>, so that it uses a dirty bit as part of the page
table to avoid writing back to disk those pages that have not been modified (whether when
evicting or when writing all pages via the <b>writeBackAllPagesToDisk()</b> method).

You will therefore have to slightly modify the log() telling if the page is dirty or not (e.g. for v3-03.log):
<pre>
[...]
10: Evicting page 8 (DIRTY)
11: Bringing page 6 into frame 0
12: RAM: 001010 <-- -103
13: Page 6 is in memory
14: RAM: 001010 --> -103
15: Page 6 is in memory
16: RAM: 001010 --> -103
17: Evicting page 24 (NOT DIRTY)
18: Bringing page 5 into frame 1
[...]
</pre>

We will consider that there is no difference between your output
logfile and the corresponding reference logfile if the "diff -wq" tool
will return nothing.

If you want to run all tests for V3, you can execute:
<pre>
java -cp hw/vmsimulation-2017050401.jar edu.hawaii.ics332.computer.tests.V3Test 
</pre>

### Question #5 [12 points EXTRA CREDIT]: Version V4 - LRU Page Replacement

In this question you must enhance your virtual memory manager, now called
<b>VirtualMemoryManagerV4</b>, so that it implements LRU rather than
FIFO page replacement. 

There is not much change in terms of log messages compared to V3
(since only the policy for eviction should change). The simulation
input files are in scenarios/V4/ and the reference logs are named
references/v4-*.log.

If you want to run all tests for V4, you can execute:
<pre>
java -cp hw/vmsimulation-2017050401.jar edu.hawaii.ics332.computer.tests.V4Test 
</pre>
