---
morea_id: experience_virtualmemoryinclass
morea_type: experience
title: "In-Class Activities"
published: True
morea_summary: ""
morea_labels: 
  - ""
---

## Paging

A computer system has a physical memory of 256 GiB. A page is 4 KiB
bytes. The logical address space of processes is limited to 128
pages. A page table entry uses 8 bytes.

 * Express the size of the computer physical memory in bytes as a
   power of 2.

 * What is the size of a physical frame in bytes?

 * How many bits are used for the offset in a page? How many
   bits are used for the offset in a frame?

 * How many bits in total are used for a virtual address in a page?
 
 * Assuming single-level paging, how many bits are used for the
   virtual page number?

 * How many pages can be referenced from one page?

 * Assuming two-level paging, how is the VPN decomposed?

## Address Translation

A process address space consists of 10 pages. The page size is 2048
bytes; The process first page is mapped to frame 24; Its second page
is mapped to frame 125. The other pages are not loaded to memory.

 * What is the last virtual address in the process address space?

 * Which physical address corresponds to logical address 26?

 * Which physical address corresponds to logical address 2049?

 * Which physical address corresponds to logical address 26049?

 * The content of a (valid) process page is referenced by the CPU but
   the page is not loaded to memory. What is the (technical) name given
   to this event?


## Incomplete page tables

Reconstruct as many page table entries as best you can based on the following facts:

 * Addresses and page/frame numbers are denoted as integers

 * Page and frame numbers start at 0

 * Page size is 2000 bytes

 * No page has been evicted from memory to date
 
 * Virtual address 3940 successfully translates to physical address 1940
 
 * Virtual address 5200 successfully translates to physical address 2800
 
 * Accessing virtual address 8842 would lead to a page fault
 
 * Content at virtual address 36 has been successfully written to at least once
 
 * No content between virtual address 1900 and 7999 has ever been written to
 
<table>
  <tr>
  <td>Page number</td>
  <td>Frame number</td>
  <td>Valid bit</td>
  <td>Dirty bit</td>
  </tr>
</table>

