---
morea_id: experience_scheduling
morea_type: experience
title: "Homework Assignment #6"
published: True
morea_summary: "Hands-On: Scheduling Forensics"
morea_labels: 
  - "Due Sun April 16th, 2017, 23:55"
---

## Assignment #6: Hands-On - Scheduling Forensics

---

You are expected to do your own work on all homework assignments.  You
may (and are encouraged to) engage in <i>general</i> discussions with
your classmates regarding the assignments, but specific details of a
solution, including the solution itself, must always be your
own work. (See the statement of Academic Dishonesty on the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>.)

#### What to turn in? 

You should turn in an electronic archive (.zip, .tar., .tgz,
etc.). The archive must contain a single top-level directory called
ics332_hwX_NAME, where "NAME" is your UH username and "X" is the
homework assignment number (e.g., ics332_hw6_schastel). Inside that
directory you should have the index.html file that will point to or
include any resource you find useful to answer the questions.

Check the <a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a> for
the late assignment policy for this course.

---

#### Environment

You can do this assignment on any platform that can run Java.

---

### Assignment Overview

For this assignment you are given a Java archive (a "jar") that
implements a simulation of a computer and an minimal kernel
(essentially a dispatcher and an event handler).

Your goal in this assignment is to figure out which particular
scheduling algorithms have been implemented.

You can download the <a download
href="hw/sp2017scheduling.jar">sp2017scheduling.jar file from here</a>.

The total number of points for this assignment is up to 21 extra-credits.

The deadline (please note that there is no late policy) is Sun April 16th, 2017.

### Scheduling Simulation

A simulation is launched as follows from the command line (you can
also launch it from whatever IDE you use, but we will test your code
from the command line):

{% highlight text %}
% java -jar sp2017scheduling.jar <algorithm> <workload file> <end of time>
{% endhighlight %}

where:

 - The first argument <algorithm> is an integer between 2 and 8 (1 is
   also supported but it is a bogus scheduler) identifying the
   scheduling algorithm to use;

 - The second argument is a workload file describing the processes to
   simulate (see below);

 - The third argument is the date (in cycles) at which the simulation
   should stop. All simulations begin at date 0.

For instance, assuming that you use the file <a download
href="hw/sample.wl"><tt>sample.wl</tt></a>, you should be able to
execute:
{% highlight text %}
% java -jar sp2017scheduling.jar 1 sample.wl 100
{% endhighlight %}

#### Workload file

The workload file is a text file that contains one line per process to
simulate. The line has five space-separated "arguments" that fully
describe the process to simulate:

 - its name (string);
 - an arrival date (integer);
 - a CPU burst length duration in cycles (integer);
 - an I/O burst length duration in cycle (integer);
 - a priority (integer between 0 and 20 inclusive).

All processes behave the same way: They are made ready at the arrival
date, then once scheduled, run on the CPU for the CPU-burst duration
(unless interrupted of course), then perform I/O during the I/O burst
duration, and loop back to the CPU burst, then the I/O burst endlessly
(hence the need for an end_of_time limit).

For instance, for the following workload:

{% highlight text %}
web_browser 20 10 100 10
{% endhighlight %}

The process named <tt>web_browser</tt> will start at t=20 (cycles),
then perform a CPU-burst of 10 (cycles), then do an I/O-burst of 100
(cycles) at a priority of 10 (whatever that priority might mean for
the scheduler).

Assuming that the description is in the file sample.wl, you
should see something like:
{% highlight text %}
% java -jar sp2017scheduling.jar 1 sample.wl 200
Setting up Simulator for algorithm [1]; workloadfile = [web_browser.wl]; endoftime = 200
[20] Process [web_browser] is READY
[20] Process [web_browser] has been DISPATCHED to the CPU
[30] Process [web_browser] has completed a CPU burst
[130] Process [web_browser] has completed an I/O burst
[130] Process [web_browser] is READY
[130] Process [web_browser] has been DISPATCHED to the CPU
[140] Process [web_browser] has completed a CPU burst
[200] End of simulation
=============================================================================
Statistics for Bogus / EOT: 200
  web_browser (arrival: 20; cpu: 10; io: 100; priority: 10): avg ready time = 0.00; cpu time = 20; #preemptions = 0
Number of context-switches/dispatches: 2
=============================================================================
{% endhighlight %}

### Questions

#### Answer Sample

For each question, in your report:

 * You will answer the question first;
 * You will show the workload file contents used to answer the question;
 * You will precise the command line that you used to answer the question;
 * You will show the part of the log that allows you to answer the question;

For instance, for the totally bogus algorithm named "42", for the second question (preemptive or not):
<pre>
Premptive or Not-Preemptive:
 * Algorithm 42 is not preemptive
 * See the file <link>42/preemptive.wl</link> whose contents are:
browser1 20 10 100 10
browser2 30 100 5 10
 * Command line for test:
java -jar sp2017scheduling.jar 42 42/preemptive.wl 10000
 * Log excerpt:
[...]
[25] This algorithm is non-preemptive
[...]
</pre>

Note: You are free to reuse the same workload if the log
contents tells you enough.

#### Restrictions

 * You will not use more than three processes (usually two
   processes will be enough to find a correct answer).

 * You will not need to have a value for &lt;end of time&gt; larger
   than 200.

#### (Real) Questions

For each algorithm 2, 3, 4, 5, 6, 7, 8:

 * [0.5pt] Is there a bug in the algorithm implementation?

 * [0.5pt] Is the priority meaningful for the algorithm?

 * [0.5pt] Is the algorithm preemptive or not preemptive?

 * [1.5pt] What is the algorithm and its parameters (if any)?

Note:

 * All algorithms have been described in class and/or in the relevant
   OSTEP chapters (see
<a href="{{ site.baseurl }}/modules/060_Scheduling/">the relevant module</a>).

