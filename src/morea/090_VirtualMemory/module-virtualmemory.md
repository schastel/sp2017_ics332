---
morea_id: 090_VirtualMemory
morea_type: module
title: "Virtual Memory - Paging"
published: True
morea_coming_soon: False
morea_highlight: False
morea_outcomes: 
  - outcomevirtualmemory
morea_readings: 
  - lecturenotes_virtualmemorypaging
  - lecturenotes_virtualmemorydemand
  - textbook_virtualmemorypaging
  - samplecode_virtualmemorypaging
morea_experiences: 
  - experience_virtualmemorysimulation
  - experience_virtualmemoryinclass
morea_assessments: 
morea_sort_order: 110
morea_icon_url: /morea/090_VirtualMemory/logo.jpg
---
