---
morea_id: processes
morea_type: module
title: "Processes"
published: True
morea_coming_soon: False
morea_highlight: False
morea_outcomes: 
  - outcomeprocesses
morea_readings: 
  - lecturenotes_processes
  - textbook_processes
  - lecturenotes_ipcs
  - samplecode_processes
morea_experiences: 
  - experience_processes
morea_assessments: 
morea_sort_order: 40
morea_icon_url: /morea/040_Processes/logo.jpg
---
