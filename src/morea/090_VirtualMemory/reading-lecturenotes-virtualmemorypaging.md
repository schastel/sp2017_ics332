---
morea_id: lecturenotes_virtualmemorypaging
morea_type: reading
title: "Virtual Memory Paging"
published: True
morea_summary: "Paging, page tables"
morea_url: /morea/090_VirtualMemory/vmp.pdf
morea_labels: 
  - "Lecture notes"
---
