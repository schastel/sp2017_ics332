---
morea_id: 020_ComputerArchitecture
morea_type: module
title: "Computer Architecture Review"
published: True
morea_coming_soon: False
morea_outcomes: 
  - outcomearch
morea_readings: 
  - lecturenotes_computerarchitecture
  - textbook_computerarchitecture_ostep
morea_experiences: 
  - experience_computerarchitecture
morea_assessments: 
morea_sort_order: 20
morea_icon_url: /morea/020_ComputerArchitecture/logo.jpg
---
