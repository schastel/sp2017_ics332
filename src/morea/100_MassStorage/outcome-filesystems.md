---
morea_id: outcomefilesystems
morea_type: outcome
title: "Storage and File Systems"
published: True
morea_sort_order: 80
---

- Know the salient characteristics of storage media
- Understand the role of file systems
- Understand how main file systems components are implemented


