---
morea_id: textbook_synchronization
morea_type: reading
title: "Textbook Reading - OSTEP"
published: True
morea_summary: "Locks; Conditional Variables; Semaphores"
morea_labels: 
  - "Reading"
---

<a href="http://pages.cs.wisc.edu/~remzi/OSTEP/threads-locks.pdf">Locks</a> - 28.1 to 28.8 and then 28.12 to 28.17

<a href="http://pages.cs.wisc.edu/~remzi/OSTEP/threads-locks-usage.pdf">Lock-based Concurrent Data Structures</a> - 29.1

<a href="http://pages.cs.wisc.edu/~remzi/OSTEP/threads-cv.pdf">Condition Variables</a> - 30.1

<a href="http://pages.cs.wisc.edu/~remzi/OSTEP/threads-sema.pdf">Semaphores</a> - 31.1-3

Note: If you think of taking ICS-432 read the integrality of these
chapters... This should give you a decent head-start.

