---
morea_id: lecturenotes_computerarchitecture
morea_type: reading
title: "Computer Architecture Review"
published: True
morea_summary: "Von Neumann model, the CPU, Fetch-Decode-Execute cycle, Memory, Multi-core"
morea_url: /morea/020_ComputerArchitecture/comparch.pdf
morea_labels: 
  - "Lecture notes"
---
