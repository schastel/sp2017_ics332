---
morea_id: lecturenotes_structures
morea_type: reading
title: "Operating System Structures"
published: True
morea_summary: "Features, interfaces, system calls, design options"
morea_url: /morea/030_OverviewAndStructures/os_structures.pdf
morea_labels: 
  - "Lecture notes"
---
