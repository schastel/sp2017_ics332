---
morea_id: experience_computerarchitecture
morea_type: experience
title: "Homework Assignment #2"
published: True
morea_summary: "A pencil-and-paper assignment to make sure that you understand the fetch-decode-execute cycle"
morea_labels: 
  - "Due Thu Feb 9th 2017, 11:55pm HST"
---

## HW#2 Pencil-and-Paper Assignment -- Computer Architecture: The Fetch-Decode-Execute cycle

---
You are expected to do your own work on all homework assignments.  You
may (and are encouraged to) engage in <i>general</i> discussions with
your classmates regarding the assignments, but specific details of a
solution, including the solution itself, must always be your
own work. (See the statement of Academic Dishonesty on the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>.)

Check the <a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a> for
the late assignment policy for this course.

This assignment gives a total of 12 points and 2 extra-credits.

#### What to turn in?

You should turn in an electronic tar gzipped archive named
<tt>ics332_hw2_UHUSERNAME.tar.gz</tt>. The archive MUST contain a
single top-level directory called ics332_hw2_UHUSERNAME, where
"UHUSERNAME" is your UH user name (e.g., for me, it would be
ics32_hw2_schastel). In that directory you should have all the
files <b>named exactly</b> as specified in the questions below.

Your report will be exclusively written in HTML and named
<tt>ics332_hw2_YourUHUserName/index.html</tt>. Your report must be
readable "as is", that is, without the reader having to perform any
operation. Points will be removed if the report is not readable.

Expected contents of the ics332_hw2_YourUHUserName directory:

 * index.html: your report

---

### Exercise #1: The Fetch-Decode-Execute Cycle [12 points + 2 extra-credits]

The purpose of this exercise is to test the understanding of the
Fetch-Decode-Execute cycle.

Consider a fictitious and very simplified Von-Neumann architecture with:

 * an 5-bit addresses/9-bit values memory
 * 2 5-bit registers for computation named A and B;
 * 1 5-bit Program Counter register;
 * 1 9-bit Current Instruction register;
 * 1 1-bit error register (set at the end of the cycle) when the execution of an instruction
   creates an "error" condition.
 * 5 instructions: LOAD, STORE, SUBTRACT, JE (Jump on Error), and STOP (detailed
   in the table below)

(Note: If you are interested in more realistic architectures, take 312 or 331) 

Each instruction is encoded using 9 bits: 

 * the first 3 bits give the mnemonics (or operation code) identifying the instruction (see table below);
 * the next bit gives the id of the register that the instruction operates on: 0 identifying A, 1 identifying B; and
 * the last 5 bits are an operand which can be anything (e.g. an integer number, an address, or even ignored) depending on the
instruction.

<table border="1">
 <thead><tr><th></th><th>CPU Instruction</th><th>Opcode</th><th>Description</th></tr></thead>
 <tbody>
 <tr><td></td><td> LOAD </td><td> 010 </td><td> Load register value with value from address (operand)
 <br/>
 Example: 010110101 =
   <font color="blue">010</font><font color="red">1</font><font color="green">10101</font>
   is executed as <font color="blue">010</font> = LOAD, <font color="red">1</font> = B,
   with the value at address (<font color="green">10101</font>, i.e. LOAD B,(10101).<br/>
   For instance, if the memory content at address 10101 is 11, then, at the end of
   the fetch-decode-execute cycle, the register B will contain the value 11.<br/>
   The value of the error register is not used. It is set to 0 when this instruction has completed its execution.
 </td></tr>
 <tr><td></td><td> STORE </td><td> 011 </td><td> Store register value to address (operand) <br/>
 Example: 011010101010 =
   <font color="blue">011</font><font color="red">0</font><font color="green">10101</font>
   is executed as <font color="blue">011</font> = STORE, <font color="red">0</font> = A,
   with the value at address (<font color="green">10101</font>, i.e. STORE A,(10101).<br/>
   For instance, if the content of the register A is d25, then, at the end of
   the fetch-decode-execute cycle, the memory location at address 10101 will contain the value 25.<br/>
   The value of the error register is not used. It is set to 0 when this instruction has completed its execution.
 </td></tr>
 <tr><td></td><td> SUB </td><td> 101 </td><td> Substract register B contents to register A contents. The result is written in A. The error register
 is set if an overflow happens<br/>
 <font color="blue">101</font><font color="black">000101</font>: SUB A, B. Note that 6 bits are ignored</br>
 If initially A contains the value 7 and B 5, then, at the end of the fetch-decode-execute cycle, A will contain 2 (=7-5), i.e. b00010.
 The error register is set to 0 (no error)</br>
 If initially A contains the value 3 and B 5, then, at the end of the fetch-decode-execute cycle, A will contain 0 (while it should be "-2" (=3-5)),
 and the error register will be set to 1 (signaling the underflow error)</br>
 The value of the error register is not used for the execution of this instruction.
 </td></tr>
 <tr><td></td><td> JE </td><td> 110 </td><td>If the error register
 value is set to 1, set the Program Counter (PC) value to (operand) if
 the error register value is set to 1. Otherwise, nothing happens (i.e. increment the PC by 1). In any case the error register value
 is set to 0 at the end of the fetch-decode-execute cycle<br/>
 For instance: <font color="blue">110</font><font color="red">0</font><font color="green">00101</font> translates
 to <font color="blue">JE</font> <font color="green">00101</font><br/>
 Case 1: If initially the error register is 0 and the PC value is 11110, then at the end of the fetch-decode-execute cycle,
 the PC value will be set to <font color="green">11111</font><br/>
 Case 2: If initially the error register is 1 and the PC value is 11110, then at the end of the fetch-decode-execute cycle,
 the PC value will be set to 00101<br/>
 The value of the error register is used for the execution of this instruction.
 </td></tr>
 <tr><td></td><td> STOP </td><td> 111 </td><td> Terminates program<br/>
 For instance: <font color="blue">111</font><font color="black">00101</font> translates to STOP. Note that all bits after
 the first three ones (00101 in this example) are ignored.<br/>
 The value of the error register is not used for the execution of this instruction.
 </td></tr>
 </tbody>
</table>

Note: The instructions encoded by 000, 001, and 100 are not described (you will not need to execute them).

<p>

<h3>Part 1: Warm-up (3 points)</h3>

<h4>Question 1.1 (1 point)</h4>
What does the instruction encoded by 110010001 do?

<h4>Question 1.2 (1 point)</h4>
Translate "LOAD A, (10110)" to binary. Show your work.

<h4>Question 1.3 (1 point)</h4> 
Explain why no program will be stored at address 0 in memory.

<h3>Part 2: Case study (9 points + 2 extra-credits)</h3>

Assume that initially:
<ul>
<li>The Program Counter value is set to 12=b01100; The values of the
   other registers are undefined.</li>
<li>The memory has the following contents:

<table border="1">
 <thead>
  <tr><th></th><th>Address</th><th>Contents</th><th></th><th>Address</th><th>Contents</th><th></th><th>Address</th><th>Contents</th><th></th><th>Address</th><th>Contents</th></tr>
 </thead>
 <tbody>
  <tr><td></td><td>00000</td><td>101110110</td><td></td><td>01000</td><td>100110001</td><td></td><td>10000</td><td>010010111</td><td></td><td>11000</td><td>001001011</td></tr>
  <tr><td></td><td>00001</td><td>110101001</td><td></td><td>01001</td><td>110010000</td><td></td><td>10001</td><td>011010110</td><td></td><td>11001</td><td>101101001</td></tr>
  <tr><td></td><td>00010</td><td>001111011</td><td></td><td>01010</td><td>010101010</td><td></td><td>10010</td><td>111000000</td><td></td><td>11010</td><td>000001011</td></tr>
  <tr><td></td><td>00011</td><td>100110110</td><td></td><td>01011</td><td>010000001</td><td></td><td>10011</td><td>001011010</td><td></td><td>11011</td><td>000000000</td></tr>
  <tr><td></td><td>00100</td><td>101000110</td><td></td><td>01100</td><td>010010110</td><td></td><td>10100</td><td>010001100</td><td></td><td>11100</td><td>100010111</td></tr>
  <tr><td></td><td>00101</td><td>010011110</td><td></td><td>01101</td><td>010110111</td><td></td><td>10101</td><td>010100000</td><td></td><td>11101</td><td>000000100</td></tr>
  <tr><td></td><td>00110</td><td>100011001</td><td></td><td>01110</td><td>101100000</td><td></td><td>10110</td><td>001000001</td><td></td><td>11110</td><td>111101101</td></tr>
  <tr><td></td><td>00111</td><td>000111011</td><td></td><td>01111</td><td>110010001</td><td></td><td>10111</td><td>010111100</td><td></td><td>11111</td><td>100100000</td></tr>
 </tbody>
</table>

</li>
</ul>

<h4>Question 2.1 (1 point)</h4> What is the address and the nature of
the first instruction executed by the program? Explain.
<p/>

<h4>Question 2.2 (7 points)</h4> Detail the program execution
instruction by instruction (the program stops after executing the first STOP).

<br/>

Detail what each stage of the Fetch-Decode-Execute cycle does for each
instruction. Make sure to describe thoroughly how the system (cpu and memory) is
or is not modified by each stage.

<p/>

<h4>Question 2.3 (1 point)</h4> What is the address the last
instruction executed by the program? Explain.

<p/>

<h4>Question 2.4 (2 extra-credits)</h4> The last STORE instruction
writes to memory the output of the program. What does this program
compute? Explain.

<br/>
Hints: Rewrite the CPU mnemonics into a higher level pseudo-language
(e.g. pseudo-C, or English). Swap the two initial input values in
memory. Play around with other values. Execute and conclude.
