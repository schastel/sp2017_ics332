---
morea_id: experience_threads
morea_type: experience
title: "Programming Assignment #5: Threads in Java"
published: True
morea_summary: "An assignment in which you experiment with Threads"
morea_labels: 
  - "Due Thu March, 16th 2017 11:55pm"
---

## Programming Assignment #5 -- Threads in Java

---

You are expected to do your own work on all homework assignments.  You
may (and are encouraged to) engage in <i>general</i> discussions with
your classmates regarding the assignments, but specific details of a
solution, including the solution itself, must always be your
own work. (See the statement of Academic Dishonesty on the
<a href="{{ site.baseurl }}/morea/010_GettingStarted/reading-syllabus.html">Syllabus</a>.)

#### What to turn in? 

You should turn in an electronic tar gzipped archive named
<tt>ics332_hw5_UHUSERNAME.tar.gz</tt>. The archive MUST contain a
single top-level directory called ics332_hw5_UHUSERNAME, where
"UHUSERNAME" is your UH user name (e.g., for me, it would be
ics332_hw5_schastel). In that directory you should have all the
files <b>named exactly</b> as specified in the questions below.

Your report will be exclusively written in HTML and named
<tt>ics332_hw5_YourUHUserName/index.html</tt>. Your report must be
readable "as is", that is, without the reader having to perform any
operation. Points will be removed if the report is not readable.

Expected contents of the ics332_hw5_YourUHUserName directory:

 * index.html: your report
 * src: the source code for the various questions

 For instance, the source code of one of the Java classes for exercise 1 will
 be the file src/edu/hawaii/ics332/flipflopgobbler/FlipFlopGobbler.java

---

#### Environment

This assignment is a Java programming assignment.

Note: You will not be graded on the performance of your
implementations but on your ability to comply with the requirements
and on your capacity of observing the behavior of your programs and
commenting on your observations.

---

### Assignment Overview

The objective of this assignment is to make you use threads in Java.

The total number of points for this assignment is 50 points and 6
extra-credits.

-------------------

## Exercise #1 [16 points + 2 extra-credits]: Stream Gobblers

Learn how to encapsulate a program in another one and play with its
output streams. You will see that from the Operating System point of
view, the programming language in which a program is implement does
not really matter.

### Part 1: 

#### Question 1.1.1 (6 points)

Implement a C program (named <tt>FlipFlop.c</tt>) that takes three
(integer) command-line arguments, <tt>nbIt</tt>, <tt>x</tt>, and
<tt>y</tt>.

When executed, the program creates two threads:

 * One thread performs <tt>nbIt</tt> iterations. An iteration consists of
printing the string "[date] flip\n" to the <b>standard output
stream</b> and then sleeping for <tt>x</tt> milliseconds

 * The other thread performs <tt>nbIt</tt> iterations. An iteration
consists of printing the string "[date] flop\n" to <b>standard error
stream</b> and then sleeping for <tt>y</tt> milliseconds

For instance, I observed the following:
<pre>
$ ./FlipFlop 5 4 2
750: flip
750: flop
752: flop
754: flip
754: flop
756: flop
758: flip
758: flop
762: flip
766: flip
</pre>

Notes:

 * You will use <tt>pthreads</tt> to implement the threads.

 * Since we are interested in millisecond resolution, feel free to
   display only the number of milliseconds in the current
   seconds. Code snippet:

   <pre>
   #include &lt;time.h&gt;
   struct timespec now;
   clock_gettime(CLOCK_REALTIME, &now);
   long ms = round(now.tv_nsec / 1.0e6); // Convert nanoseconds to milliseconds
   </pre>

 * "Sleeping" can be performed with <tt>usleep()</tt> from
   <tt>unistd.h</tt>

   (but since it has been deprecated, if you prefer, you can also use
   <tt>nanosleep()</tt>_
 
#### Question 1.1.2 (2 points)

Run your program for nbIt=10, x=2 and y=3. Show the output in your
report and comment what you see (especially things that are not
expected, e.g. in terms of timing).

#### Question 1.1.3 (1 extra-credit)

Run your program for nbIt=10000, x=2 and y=3. How can you make sure
that "flip" and "flop" are shown 10,000 times? Detail how you do it in
your report

Note: Your verification method can involve a shell script, another
program, or any program that can be run (without installing extra
packages in your Linux virtual machine). Manual verification (i.e., I
counted 10,000 flips and 10,000 flops) is obviously NOT acceptable.

### Part 2: Running a C program from Java

#### Question 1.2.1 (6 points)

Implement the FlipFlopGobbler Java class of the package
edu.hawaii.ics332.flipflopgobbler. It takes three integer command-line
arguments, <tt>nbIt</tt>, <tt>x</tt>, and <tt>y</tt>.

FlipFlopGobbler must capture the standard output stream and the
standard error stream from the FlipFlop program (run as a
ProcessBuilder from the JVM) and print them as they are produced (to
the standard output stream and to the standard error
stream). Consequently, the sequencing of flip and flop strings should
be similar to the one observed when running FlipFlop directly (there
might be a few differences in ordering and carriage returns, but the
global behavior should be similar).

To do this, you must implement a class called PipeRunnable that
implements the Runnable (or the Callable) interface:

 * Its constructor takes two parameters: one InputStream and one
   OutputStream.

 * Its run() (or call()) method reads lines from the InputStream and
   prints them to the OutputStream until there is nothing left to be
   read from the InputStream.

Hints:

 * java.io.BufferedReader is convenient to read lines from an InputStream

 * java.io.PrintStream is convenient to print lines to an OutputStream

Example:
<pre>
$ java -cp bin edu.hawaii.ics332.flipflopgobbler.FlipFlopGobbler 5 4 2
1560: flip1560: flop

1568: flip
1568: flop
1572: flop
1572: flip
1576: flop
1576: flip
1579: flip
1581: flop
</pre>

#### Question 1.2.2 (2 points)

Run the FlipFlopGobbler program for nbIt=10, x=2 and y=3. Show the
output in your report and verify that the output is similar to the
output of the FlipFlop program (comment on the differences if any).

#### Question 1.2.3 (1 extra-credit)

Describe how you can count the number of "flip" (or "flop") messages
in the PipeRunnable run() (or call() method).

-------------------

## Exercise #2 [34 points + 4 extra-credits]: Speeding-Up-A-Computation-With-Threads-101 

The purpose of this exercise is to show you how a computation can be
sped up using threads. If you want to delve into the depths of
computational performance, ICS-432 will be your next step.

### Part 1: A point in two dimensions

The objective here is to implement a class whose instances represent
points in the 2D-plane, to compute the Euclidian distance between two
points, and to generate a random point in the rectangle delimited by
[0:a]x[0:b].

#### Question 2.1 (2 points)

Implement the <tt>Point2D</tt> Java class in the
<tt>edu.hawaii.ics332.hw5.models</tt> package. Implement the only
constructor of that class, namely <tt>Point2D(double x, double y)</tt>
that stores a point of the 2D plane (identified by its Cartesian
coordinates x and y).

For convenience, the point instantiated by <tt>new Point2D(x,y)</tt>
will be denoted by (x,y) in the following.

#### Question 2.2 (2 points)

Add to Point2D the <tt>double distance(Point2D other)</tt> method that
returns the Euclidian distance between <tt>this</tt> point and an
<tt>other</tt> one. You will provide a few unit tests to assert (1)
that the distance of a point to itself is 0; (2) that the distance
between (0,0) and (1,1) is &#x221A;2; (3) that the distance between
(42,0) and (0,42) is equal to the distance between (0,42) and (42,0).

Note: The Euclidian distance between (x1, y1) and (x2, y2) is given by
Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)).

#### Question 2.3 (2 points)

Add to Point2D the <tt>public static Point2D random(java.util.Random random,
double a, double b)</tt> method that creates a random point of the
[0,a]x[0,b] rectangle.

### Part 2: Average distance of a set of points

#### Question 2.4 (2 points)

Implement the <tt>AverageDistanceCalculator</tt> Java class in the
<tt>edu.hawaii.ics332.hw5.operators</tt> package.

 * Its constructor takes two ArrayList of Point2D (ArrayList&lt;Point2D&gt;)

 * The compute() method computes and returns the average distance
   between any pair of points built from each of the two collections.

   (The average distance is the sum of all the distances divided by
   the number of pairs)

#### Question 2.5 (4 points)

Test your code (i.e. write a main() method that takes the number of
points as argument) and report the values and the execution time for
the rectangle [0:2]x[0:5] for the following cases:

 * Average distance between two collections of 10 random Point2D

 * Average distance between two collections of 100 random Point2D

 * Average distance between two collections of 1000 random Point2D

 * Average distance between two collections of 10000 random Point2D

Note: Detail in your report how to run the tests from the command line.

Note: To get an idea of the execution time, you can use code like:
<pre>
  java.time.ZonedDateTime begin = ZonedDateTime.now();
  /*
   * Do some computation here
   */
  java.time.ZonedDateTime end = ZonedDateTime.now();
  Duration executionTime = Duration.between(begin, end);
  long executionTimeInSeconds = executionTime.toSeconds();
</pre>

#### Question 2.6 (1 extra-credit)

Give an estimate of the expected execution time for two collections of
100,000 points. Same question for 1,000,000 points. Explain your work.

### Part 3: Multi-threading

#### Question 2.7 (6 points)

Implement the <tt>AverageDistanceCalculatorCallable</tt> Java class in the
<tt>edu.hawaii.ics332.hw5.operators</tt> package.

 * It implements a Callable<Double>

 * The call() method returns the value provided by the AverageDistanceCalculator::compute() method.
 
#### Question 2.8 (2 points)

Write a main() method that will check that the result given by the
execution of an AverageDistanceCalculatorCallable spawned by an
ExecutorService of your choice is the same that what an
AverageDistanceCalculator instance on the same collections of Point2D
provides.

#### Question 2.9 (10 points)

Write a class implementing a method that will split ArrayList&lt;Point2D&gt;
into smaller ArrayList of size at most 10,000 and create an
<tt>AverageDistanceCalculatorCallable</tt> instance for each pair of
sublists.

Note: Using the method ArrayList::subList will be a good idea.

#### Question 2.9 (4 points)

Use that class to compute the average distance of two collections of
100,000 points, of 1,000,000 points and evaluate the global execution
time of your program using an ExecutorService set up by
Executors.newFixedThreadPool(nbThreads) where nbThreads is the number
of CPUs you observed in HW1. Report those execution time in your report.

What do you observe? Explain.

#### Question 2.10 (3 extra-credits)

Use an ExecutorService instance created using
Executors.newFixedThreadPool(nbThreads) to spawn the various Callable
threads, evaluate the execution time for collections of 1,000,000
points

Note: Use 100,000 points if the computation time is too large,
i.e. more than 15 minutes, on your platform.

Plot the execution time (in seconds) vs the value of nbThreads (for
nbThreads = 2, 3, ..., ). What do you observe? Explain what
happens.
