---
morea_id: textbook_processes
morea_type: reading
title: "Textbook Reading"
published: True
morea_summary: "Processes"
morea_labels: 
  - "Reading"
---

[The Process](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-intro.pdf)

[The Process API](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-api.pdf)

[Mechanism: Limited Direct Execution](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-mechanisms.pdf)

[Distributed Systems](http://pages.cs.wisc.edu/~remzi/OSTEP/dist-intro.pdf)
