---
morea_id: lecturenotes_virtualmemorydemand
morea_type: reading
title: "Virtual Memory Management"
published: True
morea_summary: "Segmentation, Demand Paging, Memory Mapping"
morea_url: /morea/090_VirtualMemory/vms.pdf
morea_labels: 
  - "Lecture notes"
---
