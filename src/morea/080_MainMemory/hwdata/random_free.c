#include <stdlib.h>
#include <stdio.h>

int main() {
  int length = 100;
  int *p;
  p = (int *) malloc(length*sizeof(int));

  for (int i=0;i<length;i++) {
    p[i] = 10*i;
  }

  int *theFifthElement = p+4;
  printf("Address for %d is %p\n", *theFifthElement, theFifthElement);
  
  free(theFifthElement);
  
  return 0;
}

