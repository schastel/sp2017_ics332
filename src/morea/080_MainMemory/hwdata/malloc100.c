#include <stdlib.h>
#include <stdio.h>

int main() {
  int length = 100;
  int *p;
  p = (int *) malloc(length*sizeof(int));

  for (int i=0;i<length;i++) {
    p[i] = 10*i;
  }
  p[100] = -1;
  
  printf("%d\n", 5[p]);
  free(p);
  return 0;
}

