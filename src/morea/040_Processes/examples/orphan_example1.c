#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  pid_t pidChild;

  if ((pidChild = fork()) < 0) {
    fprintf(stderr,"Error: can't fork a process. Aborting\n");
    exit(-1);
  } else if (pidChild == 0) {
    // In first child
    int maxIterations = 400;//Just to make sure the child terminates at some point
    while (maxIterations > 0) {
      printf("I'm the child and I'm running (%d iterations left)\n", maxIterations);
      sleep(4);
      maxIterations--;
    }
    exit(0);
  } else {
    // In parent
      printf("I'm the parent and I'm running\n");
      sleep(5);
  }
  printf("I'm the parent and I'm terminating\n");
}
