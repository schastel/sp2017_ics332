---
morea_id: 070_Synchronization
morea_type: module
title: "Synchronization"
published: True
morea_coming_soon: False
morea_highlight: False
morea_outcomes: 
  - outcomesynchronization
morea_readings: 
  - lecturenotes_synchronization
  - textbook_synchronization
  - samplecode_sync
  - lecturenotes_conbugs
  - textbook_conbugs
#  - samplecode_conbugs
#morea_experiences: 
#  - experience_jsh3
morea_assessments: 
morea_sort_order: 80
morea_icon_url: /morea/070_Synchronization/logo.jpg
---

