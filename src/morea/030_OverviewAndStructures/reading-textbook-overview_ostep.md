---
morea_id: textbook_overview
morea_type: reading
title: "Textbook Readings"
published: True
morea_summary: ""
morea_labels: 
  - "Reading"
---

## Textbook Readings

### -> <a target="_blank" href="http://pages.cs.wisc.edu/~remzi/OSTEP/intro.pdf">Introduction</a>

### -> <a target="_blank" href="http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-mechanisms.pdf">Protected Mode, System calls...</a>

