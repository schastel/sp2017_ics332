#include <time.h>

char TIMESTAMP[256];

/**
 * Returns current date/time UTC as string
 *
 * printf("%s: Blah\n", timestamp());
 */
const char* timestamp() {
  time_t now;
  time(&now);
  strftime(TIMESTAMP, sizeof(TIMESTAMP), "%FT%TZ", gmtime(&now));
  return TIMESTAMP;
}

/**
 * Pseudo-random generator for the "Hot Potato" exercise
 * Generates the sequence: 0 1 5 3 4 8 6 7
 */
int hotPotato(int currentPotato) {
  return (4*currentPotato+1) % 9;
}

